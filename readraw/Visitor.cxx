//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Visitor.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:32:42 2006
    @brief   Implementation of visitor base class 
*/
#include "Visitor.h"
#include "Event.h"
#include "Header.h"
#include "EventId.h"
#include "AttributePattern.h"
#include "Equipment.h"
#include "CommonHeader.h"
#include <iostream>

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const Event& event) 
{
  static unsigned long evno = 0;
  if (!Visit(event.Head())) {
    std::cerr << "ReadRaw::Visitor::Visit(Event): No head for event " 
	      << evno << " in range" << std::endl;
    evno++;
    return false;
  }
  for (Event::SubMap::const_iterator i = event.Sub().begin(); 
       i != event.Sub().end(); ++i) {
    if (!Visit(*(*i).second)) {
      std::cerr << "ReadRaw::Visitor::Visit(Event): Failed on sub-event of "
		<< " event " << evno << " in range" << std::endl;
      evno++;
      return false;
    }
  }
  evno++;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const Header& header) 
{
  if (!Visit(header.Id())) {
    std::cerr << "ReadRaw::Visitor::Visit(Header): Failed on Id" << std::endl;
    return false;
  }
  if (!Visit(header.Triggers())) {
    std::cerr << "ReadRaw::Visitor::Visit(Header): Failed on triggers" 
	      << std::endl;
    return false;
  }
  if (!Visit(header.Detectors())) {
    std::cerr << "ReadRaw::Visitor::Visit(Header): Failed on Detectors" 
	      << std::endl;
    return false;
  }
  if (!Visit(header.Attributes())) {
    std::cerr << "ReadRaw::Visitor::Visit(Header): Failed on Attributes" 
	      << std::endl;
    return false;
  }
  
  for (Header::EquipmentMap::const_iterator i = header.Equipments().begin(); 
       i != header.Equipments().end(); ++i) {
    if (!Visit(*(*i).second)) {
      std::cerr << "ReadRaw::Visitor::Visit(Header): Failed on Equipment" 
		<< std::endl;
      return false;
    }
  }
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const EventId& id) 
{
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const TriggerPattern& triggers) 
{
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const DetectorPattern& detectors) 
{
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const AttributePattern& attributes) 
{
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const Equipment& equipment)
{
  if (!Visit(equipment.Attributes())) {
    std::cout << "ReadRaw::Visitor::Visit(Equipment): Failed on Attributes"
	      << std::endl;
    return false;
  }
  if (!Visit(equipment.Head())) {
    std::cout << "ReadRaw::Visitor::Visit(Equipment): Failed on Head"
	      << std::endl;
    return false;
  }
  if (!Visit(equipment.Data(), equipment.DataSize())) {
    std::cout << "ReadRaw::Visitor::Visit(Equipment): Failed on Data"
	      << std::endl;
    return false;
  }
  return true;  
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const CommonHeader& attributes) 
{
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Visitor::Visit(const uint32_t* data, const uint32_t size) 
{
  return true;
}

//____________________________________________________________________
//
// EOF
//

  





  
