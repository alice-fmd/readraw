//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Reader.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:32:06 2006
    @brief   Implementation of reader abstract base class 
*/
#include "Reader.h"
#include "FileReader.h"
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifdef HAVE_DATE
# include "Monitor.h"
#endif
#ifndef VERSION
# define VERSION "?.?"
#endif
#include <sstream>
#include <iostream>

namespace 
{
  const std::string versionString = VERSION;
}

//____________________________________________________________________
int
ReadRaw::Reader::Parse(const std::string& url, std::string& toopen)
{
  int         ret = 0;
  size_t      start;
  if ((start = url.find("date://")) != std::string::npos) {
    size_t      cur = start + 7;

    size_t      pipe = url.find("|", cur);
    std::string relay;
    if (pipe != std::string::npos) {
      relay = url.substr(cur, pipe - cur);
      cur   = pipe+1;
    }
    
    size_t      slash = url.find("/", cur);
    std::string host;
    if (slash == std::string::npos && cur < url.size())
      slash = url.size();

    if (slash != std::string::npos && slash != cur + 1) {
      host = url.substr(cur, slash - cur);
      cur  = slash + 1;
      std::cout << "=> Got host " << host << std::endl;
    }

    std::string file;
    if (cur < url.size()) {
      file = url.substr(cur, url.size() - cur);
      std::cout << "=> Got file name " << file << std::endl;
    }

#ifndef HAVE_DATE
    std::cerr << "DATE URI '" << url << "' not supported" << std::flush;
    if (host.empty() && relay.empty() && !file.empty()) {
      std::cerr << ", but since you specified a file only, I'll go on anyway." 
		<< std::endl;
      toopen = file;
      ret    = 1;
    }
    else {
      std::cerr << "." << std::endl;
      ret = 0;
    }
#else
    ret = 2;
    std::stringstream s;
    if (!file.empty()) s << file;
    if (!host.empty()) {
      s << "@" << host;
      if (!relay.empty()) 
	s << "@" << relay;
    }
    if (file.empty()) s << ":";
    toopen = s.str();
#endif
  }
  else {
    toopen = url;
    ret    = 1;
  }
  return ret;
}
  
//____________________________________________________________________
ReadRaw::Reader* 
ReadRaw::Reader::Create(const std::string& url, bool verbose, bool debug) 
{
  std::string toopen;
  int         type   = Parse(url, toopen);
  Reader*     ret    = 0;
  switch (type) {
  case 1: 
    ret    = new FileReader;
    break;
#ifdef HAVE_DATE
  case 2: 
    ret    = new Monitor;
    break;
#endif
  }
  if (ret && !toopen.empty()) {
    ret->SetVerbose(verbose);
    ret->SetDebug(debug);
    if (!ret->SetInput(toopen.c_str())) {
      delete ret;
      ret = 0;
    }
  }
  return ret;
}

//____________________________________________________________________
ReadRaw::Reader* 
ReadRaw::Reader::Create(const std::vector<std::string>& urls, bool verbose, bool debug) 
{
  if (urls.empty()) {
    std::cerr << "No inputs specified" << std::endl;
    return 0;
  }
  
  std::string url    = *(urls.begin());
  Reader*     ret    = new FileReader;
  if (!ret) return ret;
  ret->SetVerbose(verbose);
  ret->SetDebug(debug);
  if (!ret->SetInput(urls)) {
    delete ret;
    ret = 0;
  }
  return ret;
}

//____________________________________________________________________
ReadRaw::Reader::Reader(const Reader& other) 
  : fMask(other.fMask), 
    fVerbose(other.fVerbose), 
    fDebug(other.fDebug)
{
  if (!other.fMask) return;
  if (dynamic_cast<const AttributeMask*>(other.fMask)) 
    SetMask(*(dynamic_cast<const AttributeMask*>(other.fMask)));
  else 
    SetMask(*(other.fMask));
}

//____________________________________________________________________
ReadRaw::Reader&
ReadRaw::Reader::operator=(const Reader& other) 
{
  if (!other.fMask) return *this;
  if (dynamic_cast<const AttributeMask*>(other.fMask)) 
    SetMask(*(dynamic_cast<const AttributeMask*>(other.fMask)));
  else 
    SetMask(*(other.fMask));
  return *this;
}

//____________________________________________________________________
ReadRaw::uint32_t
ReadRaw::Reader::SwapBytes(uint32_t w) 
{
  return ((w >> 24 & 0x000000ff) || 
	  (w >>  8 & 0x0000ff00) || 
	  (w <<  8 & 0x00ff0000) || 
	  (w << 24 & 0xff000000));
}

//____________________________________________________________________
ReadRaw::uint32_t
ReadRaw::Reader::SwapWords(uint32_t w) 
{
  return ((w >> 16 & 0x0000ffff) || (w << 16 & 0xffff0000));
}

//____________________________________________________________________
const std::string&
ReadRaw::Reader::Version()
{
  return versionString;
}


//____________________________________________________________________
//
// EOF
//  
  

  

