//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AltroPrinter.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:27:30 2006
    @brief   Implementation of ALTRO printer
*/
#include "AltroPrinter.h"
#include "Header.h"
#include <iostream>
#include <iomanip>
#include <cmath>

//____________________________________________________________________
bool 
ReadRaw::AltroPrinter::Visit(const Header& header)
{
  fIsSodEod = (header.Type() == 10  || header.Type() == 11);
  return Printer::Visit(header);
}

//____________________________________________________________________
bool
ReadRaw::AltroPrinter::Visit(const Equipment& equipment)
{
  fDDL = equipment.Id();
  return Printer::Visit(equipment);
}

//____________________________________________________________________
bool
ReadRaw::AltroPrinter::Visit(const CommonHeader& h) 
{ 
  fFormatVersion = (h.Attributes() & 0xff);
  return Printer::Visit(h); 
}

#define CH_OUT(B,A,C,L) " DDL: " << std::setw(4) << (fDDL)	   \
  << "  Board: "   << std::setw(2) << (B) 			   \
  << "  Chip: "    << std::setw(1) << (A)			   \
  << "  Channel: " << std::setw(2) << (C)			   \
  << "  Last: "    << std::setw(4) << (L)  

//____________________________________________________________________
bool
ReadRaw::AltroPrinter::Visit(const uint32_t*  data, const uint32_t   size) 
{
  if (fMask & kData) Printer::Visit(data, size);
  if (!(fMask & (kData << 1)) && !(fMask & (kData << 5))) return true;
  if (fIsSodEod) { 
    if (!AltroDecoder::DecodeSodEod(data, size, fFormatVersion)) return false;
    // return true;
  }
  else if (!AltroDecoder::DecodeEvent(data, size, fFormatVersion)) { 
    std::cerr << "Failed to decode event" << std::endl;
    return false;
  }

  SpacerGuard g(fSpacer);
  if ((fMask & (kData << 5))) {
    fOut << fSpacer << "Trailer: " << fTrailer.size() << " entries" <<std::endl;
    for (TrailerMap::iterator i = fTrailer.begin(); i != fTrailer.end(); ++i) 
      PrintTrailerWord(i->first, i->second);
  }

  if (fIsSodEod) return true;

  if (!(fMask & (kData << 1))) return true;
  fOut << fSpacer << "Channel data: (" << fMap.size() << " channels)" 
       << std::endl;
  size_t nChannels = 0;
  for (ChannelMap::const_iterator i = fMap.begin(); i != fMap.end(); i++) {
    const AltroChannel* c = i->second;
    nChannels++;
    fOut << fSpacer 
	 << std::setw(3) << nChannels << ": " 
	 << CH_OUT(c->fBoard, c->fChip, c->fChannel, c->fMaxT) 
	 << " " << c->fMinT << std::endl;
    if (!(fMask & (kData << 2))) continue;
    SpacerGuard g2(fSpacer);
    size_t i = 0;
    for (uint16_t t = c->fMinT; t <= c->fMaxT; t++, i++) {
      if (i != 0 && i % 4 == 0) fOut << std::setfill(' ') << std::endl;
      if (i % 4 == 0) 
	fOut << fSpacer << std::dec << std::setw(4) << t 
	     << "-" << std::setw(4) << t+3 << ":"   << std::hex 
	     << std::setfill('0');
      fOut << " 0x" << std::setw(3) << c->fData[t];
    }
    fOut << std::setfill(' ') << std::dec << std::endl;
  }
  return true;
}

#define PRINT_HEX(X,N) "0x" \
  << std::hex << std::setfill('0') << std::setw(size_t(N/4+.5)) << X	\
  << std::dec << std::setfill(' ')
  
//____________________________________________________________________
void 
ReadRaw::AltroPrinter::PrintTrailerWord(unsigned short addr, 
					unsigned int val)
{
  switch (addr) { 
  case 0x0:  
    fOut << fSpacer << "Payload length:\t" << val << std::endl;
    break;
  case 0x1: {
    fOut << fSpacer << "Errors (part 1): " << PRINT_HEX(val,26) << "\n";
    SpacerGuard g(fSpacer);
    for (size_t i = 0; i < 2; i++) { 
      unsigned short err = ((val >> (i*13)) & 0x1FFF);
      if (err == 0) continue;
      fOut << fSpacer << "Branch " << (i == 0 ? 'A' : 'B') << std::endl;
      SpacerGuard g1(fSpacer);
      for (size_t j = 0;  j < 13; j++) { 
	if (!(err & (1 << j))) continue;
	switch (j) { 
	case  0: fOut << fSpacer << "Error during broadcast\n"; break;
	case  1: fOut << fSpacer << "Error during assert\n"; break;
	case  2: fOut << fSpacer << "Error during acknowledge\n"; break;
	case  3: fOut << fSpacer << "Acknowledge before command\n"; break;
	case  4: fOut << fSpacer << "Error while waiting for ackn\n"; break;
	case  5: fOut << fSpacer << "No acknowledge\n"; break;
	case  6: fOut << fSpacer << "Error while waiting for release\n"; break;
	case  7: fOut << fSpacer << "Infinite acknowledge\n"; break;
	case  8: fOut << fSpacer << "Write error during readout\n"; break;
	case  9: fOut << fSpacer << "Error before transfer\n"; break;
	case 10: fOut << fSpacer << "No transfer\n"; break;
	case 11: fOut << fSpacer << "Error during transfer\n"; break;
	case 12: fOut << fSpacer << "Infinite transfer\n"; break;
	}
      }
    }
  }
    break;
  case 0x2: {
    fOut << fSpacer << "Errors (part 2): " << PRINT_HEX(val,26) << "\n";
    SpacerGuard g(fSpacer);
    if (val & (1 << 5)) fOut << fSpacer << "Scan event length error\n";
    if (val & (1 << 6)) fOut << fSpacer << "Ready-to-recieve error\n";
    if (val & (1 << 7)) fOut << fSpacer << "Channel address mismatch\n";
    if (val & (1 << 8)) fOut << fSpacer << "Block length mismatch\n";
    if (val & 0x1F)     
      fOut << fSpacer << "Event length error: 0x"  << std::hex 
	   << (val & 0x1F) << std::dec << "\n";
  }
    break;
  case 0x3:{
    fOut << fSpacer << "Errors (part 3): " << PRINT_HEX(val,26) << "\n";
    SpacerGuard g(fSpacer);
    fOut << fSpacer << "# of channel address mismatches:\t" 
	 << (val & 0xFFF) << "\n"
	 << fSpacer << "# of channel length mismatches:\t" 
	 << ((val >> 12) & 0x1FFF) << "\n";
  }
    break;
  case 0x4:
  case 0x5:
    fOut << fSpacer << "Active FECs branch " << (addr == 0x5 ? 'A' : 'B')
	 << ":\t\t\t0x" << std::hex << std::setfill('0') << std::setw(4) 
	 << (val & 0xFFFF) << std::dec << std::setfill(' ') << std::endl;
    break;
  case 0x6:
    fOut << fSpacer << "Read-out configuration (part 1): " 
	 << PRINT_HEX(val,26) << "\n"
	 << fSpacer << "  1st baseline mode:\t\t\t"    << ((val>> 0)&0xf)<< "\n"
	 << fSpacer << "  2nd baseline pre:\t\t\t"     << ((val>> 4)&0x3)<< "\n"
	 << fSpacer << "  2nd baseline post:\t\t\t"    << ((val>> 6)&0xf)<< "\n"
	 << fSpacer << "  2nd baseline enabled:\t\t\t" << ((val>>10)&0x1) <<"\n"
	 << fSpacer << "  ZS glitch filter:\t\t\t"     << ((val>>11)&0x3)<< "\n"
	 << fSpacer << "  ZS pre:\t\t\t\t" 	       << ((val>>13)&0x7)<< "\n"
	 << fSpacer << "  ZS post:\t\t\t\t"            << ((val>>16)&0x3)<< "\n"
	 << fSpacer << "  ZS enabled:\t\t\t\t"         << ((val>>18)&0x1)
	 << std::endl;
    break;
  case 0x7:
    fOut << fSpacer << "Read-out configuration (part 2): " 
	 << PRINT_HEX(val,26) << "\n"
	 << fSpacer << "  Phase of L1:\t\t\t\t"   << ((val>> 0) & 0x1F)  << "\n"
	 << fSpacer << "  Sample clock:\t\t\t\t"  << ((val>> 5) & 0x0F)  << "\n"
	 << fSpacer << "  Sparse readout:\t\t\t"  << ((val>> 9) & 0x01)  << "\n"
	 << fSpacer << "  # of samples/ch:\t\t\t" << ((val>>10) & 0x3FF) << "\n"
	 << fSpacer << "  # of pre-samples:\t\t\t"<< ((val>>20) & 0xF)   << "\n"
	 << fSpacer << "  # of ALTRO buffers:\t\t\t" 
	 << (((val >> 24) & 0x1) ? 8 : 4) << std::endl;
    break;
  case 0x9:
    fOut << fSpacer << "Identifier: " << PRINT_HEX(val,26) << "\n"
	 << fSpacer << "  Trailer length:\t" << ((val >>  0) & 0x7F)  << "\n"
	 << fSpacer << "  RCU identifier:\t" << ((val >>  7) & 0x1FF) 
	 << std::endl;
    break;
  }
}

//____________________________________________________________________
// 
// EOF
//

