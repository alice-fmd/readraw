// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    DetectorPattern.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 21:06:21 2006
    @brief   Implementation
*/
#ifndef READRAW_DETECTORPATTERN_H
#define READRAW_DETECTORPATTERN_H
#ifndef READRAW_PATTERN_H
# include <readraw/Pattern.h>
#endif

namespace ReadRaw
{
  //__________________________________________________________________
  /** @struct DetectorPattern readraw/DetectorPattern.h <readraw/DetectorPattern.h>
      @ingroup Data
      Pattern of detectors */

  struct DetectorPattern : public Pattern
  {
    /** Constructor
	@param data Data 
	@param version Version of event. */
    DetectorPattern(uint32_t* data=0, uint32_t version=0) 
      : Pattern(data, version)
    {}
    /** The number of words */ 
    size_t N() const { return 1; }
    /** Smallest bit */
    size_t Min()  const { return fVersion < 0x00030009 ?  1 :  0; }
    /** Largets bit */
    size_t Max()  const { return fVersion < 0x00030009 ? 24 : 30; }
    /** Is pattern OK? */
    bool IsOk() const 
    { 
      return (fData[0] & (fVersion < 0x00030009 
			  ? 0xfe0000 : 0x3f000000)) == 0; 
    }
    /** Bits corresponding to detectors */
    enum {
      /** Bit corresponding to ITS_SPD (0) */
      ITS_SPD = 0,
      /** Bit corresponding to ITS_SDD (1) */
      ITS_SDD,
      /** Bit corresponding to ITS_SSD (2) */
      ITS_SSD,
      /** Bit corresponding to TPC (3) */
      TPC,    
      /** Bit corresponding to TRD (4) */
      TRD,    
      /** Bit corresponding to TOF (5) */
      TOF,    
      /** Bit corresponding to HMPID (6) */
      HMPID,  
      /** Bit corresponding to PHOS (7) */
      PHOS,   
      /** Bit corresponding to CPV (8) */
      CPV,    
      /** Bit corresponding to PMD (9) */
      PMD,    
      /** Bit corresponding to MUON_TRK (10) */
      MUON_TRK,
      /** Bit corresponding to MOUN_TRG (11) */
      MOUN_TRG,
      /** Bit corresponding to FMD (12) */
      FMD,     
      /** Bit corresponding to T0 (13) */
      T0,      
      /** Bit corresponding to V0 (14) */
      V0,      
      /** Bit corresponding to ZDC (15) */
      ZDC,     
      /** Bit corresponding to ACORDE (16) */
      ACORDE,  
      /** Bit corresponding to TRG (17) */
      TRG,     
      /** Bit corresponding to EMCAL (18) */
      EMCAL,   
      /** Bit corresponding to HLT (19) */
      HLT,     
      /** Bit corresponding to DAQ_TEST (30) */
      DAQ_TEST = 30
    };
  };
}

#endif
//
// EOF
//
