// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Visitor.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Fri Jun 23 01:29:49 2006
    @brief   Declaration of visitor class 
*/
#ifndef READRAW_VISITOR_H
#define READRAW_VISITOR_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef READRAW_TRIGGERPATTERN_H
# include <readraw/TriggerPattern.h>
#endif
#ifndef READRAW_DETECTORPATTERN_H
# include <readraw/DetectorPattern.h>
#endif

namespace ReadRaw 
{
  /* Forward declarations */
  struct Event;
  struct EventId;
  struct Header;
  struct AttributePattern;
  struct Equipment;
  struct CommonHeader;
  
  /** Visitor of raw event 
      @ingroup Reader
   */
  struct Visitor
  {
    /** Destructor */
    virtual ~Visitor() {}
    /** Visit an event.  Calls Visit on header, and header of each
	sub-event. 
	@param event The Event object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const Event&                      event);
    /** Visit a header.  Calls Visit of each contained equipment. 
	@param header The event Header object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const Header&                     header);
    /** Visit an event ID of a header. 
	@param id The EventId object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const EventId&                    id);
    /** Visit a trigger pattern. 
	@param triggers The trigger pattern object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const TriggerPattern&             triggers);
    /** Visit a detector pattern. 
	@param detectors The detector pattern object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const DetectorPattern&            detectors);
    /** Visit a attribute pattern. 
	@param attributes The attribute pattern object to visit.
	@return @c true on success, @c false otherwise  */
    virtual bool Visit(const AttributePattern&           attributes);
    /** Visit an equipment.  Calls Visit on the common data header,
	and the data of the equipment. 
	@param equipment Equipment object to visit.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const Equipment&                  equipment);
    /** Visit a common data header. 
	@param header The CommonHeader object to visit. 
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const CommonHeader&               header);
    /** Visit the data of an equipment.
	@param data Array of data words, packed as 32bit words. 
	@param size Number of 32bit words in @a data
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const uint32_t* data, const uint32_t size);
  };
}

#endif
//
// EOF
//
  
  
