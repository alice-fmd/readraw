// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Reader.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Jun 22 19:55:25 2006
    @brief   
*/
#ifndef READRAW_READER_H
#define READRAW_READER_H
#ifndef READRAW_EVENT_H
# include <readraw/Event.h>
#endif
#ifndef READRAW_MASK_H
# include <readraw/Mask.h>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

/** @namespace ReadRaw 
    Name space of all classes in this library
 */
namespace ReadRaw
{
  /** @defgroup Reader Reader related classes 
   */
  //==================================================================
  /** Abstract base class for readers
      @ingroup Reader
   */
  struct Reader 
  {
    /** Static function to create a reader 
	@param verbose Wether to be verbose or not. 
	@param url  URL describing the stream to open.  This can have
	formats 
	
	@e filename @n
	@b date:// [[@e relay_host @b |]@e host ][@b /@e filename ]@n 

	In the first case, a file on available on the local machine is
	opened for reading, and all events are processed.   If the
	file name contains the string @c .000 then it's assumed to be
	part of a multi-file run, and the reader will read in as many
	files as it possibly can.  The files should all be in the same
	directory. 

	In the latter case, a @b DATE monitor is set up, if supported.

	If @e filename is not given, the monitor will monitor on-line
	events.  If @e filename is specified, the corresponding file
	is opened and all events processed.

	@e host is optional, but if given, the monitor will listen for
	events from that host, whether it's a file on the remote host,
	or on-line events.  Note, that the file name should probably
	absolute. 

	@e relay_host is optional, but if givem, then the monitor will
	listen for events from @e host via the relay host @e
	relay_host. Please refer to the @b DATE documentation for how
	to set this up. 
	
	Some examples are of local reading from file(s) are 
	@verbatim 
	run_111.dat                              # Single local file
	/data/run_112.000.dat                    # Multiple local files
	@endverbatim 

	Some examples of monitoring are 
	@verbatim
	date://                                  
	date://run_113.dat                       
        date://daq01.alice.cern.ch               
	date://daqgw.cern.ch|daq01.alice.cern.ch
	@endverbatim. 
	- First line is a local on-line monitor. 
	- Second line is local file via monitor.
	- Third line is remote on-line monitor of @c
	daq01.alice.cern.ch
	- Fourth line is remote on-line monitor of @c
	daq01.alice.cern.ch via the relay host @c daqgw.cern.ch. 
	@return Newly allocated reader, or 0 in case of failure. 
    */
    static Reader* Create(const std::string& url, bool verbose=false, bool debug=false);

    /** Create a reader, reading only the specified files.  See also
	the other Create class member function. 
	@param urls List of files to read 
	@param verbose Wether to be verbose or not.
	@return Newly allocated reader, or 0 in case of failure. 
    */
    static Reader* Create(const std::vector<std::string>& urls, 
			  bool verbose=false, bool debug=false);
    
    /** Destructor */
    virtual ~Reader() {}
    
    /** Get the next event. 
	@return If an error occured, or the event was not selected by
	the mask, then retur 0.  If an error occured, IsEOD will
	return true. */
    virtual bool GetNextEvent(Event& e) = 0;
    /** Add an input file 
	@param input Input source stream */
    virtual bool AddInput(const char* input) { return SetInput(input); }
    /** Set the input source stream 
	@param input Input source stream */
    virtual bool SetInput(const char* input) { return false; }
    /** Set the input source stream 
	@param input Input source stream */
    virtual bool SetInput(const std::vector<std::string>& input){return false;}
    /** Set the mask to use 
	@param m Mask to use  */
    virtual void SetMask(const Mask& m) { fMask = &m; }
    /** Set the mask to use 
	@param m Mask to use  */
    virtual void SetMask(const AttributeMask& m) { fMask = &m; }
    /** Remove the mask */
    virtual void RemoveMask() { fMask = 0; }
    /** Set whether to wait or not */
    virtual void SetWait(bool on=true) {}
    /** Flush stored events */
    virtual void Flush() {}
    /** @return Return @a true if no more data can be read */
    virtual bool IsEOD() = 0;
    /** Utility function to swap bytes of a 32 bit word 
	@param w Word to swap bytes in.
	@return @a w byte swapped */
    static uint32_t SwapBytes(uint32_t w);
    /** Utility function to swap 2 bytes of a 32 bit word 
	@param w Word to swap 2 bytes in.
	@return @a w 2 byte swapped */
    static uint32_t SwapWords(uint32_t w);
    /** Set verbose */
    void SetVerbose(bool verb=true) { fVerbose = verb; }
    /** Set debug */
    void SetDebug(bool verb=true) { fDebug = verb; }
    /** Get verbose */
    bool IsVerbose() const { return fVerbose; }
    /** Get (an estimate of) the total number of events.  Returns -1
	one for @e don't @e know */
    virtual long Events() const { return -1; }
    /** Get the version number as a string 
	@return Version number as a string */ 
    static const std::string& Version();
  protected:
    /** Class function to check type of file to open 
	@param url URL of file to read 
	@param toopen URL to open 
	@return 1 for FileReader, 2 for Monitor, or 0 if no applicable
	handler is found. */
    static int Parse(const std::string& url, std::string& toopen);
    
    /** Constructor */
    Reader() :fMask(0), fVerbose(false), fDebug(false) {}
    /** Copy constructor 
	@param other Object to copy from */
    Reader(const Reader& other);
    /** Assignment operator. 
	@param other Object to assign from 
	@return Reference to this object */
    Reader& operator=(const Reader& other);
    /** Mask to use */
    const Mask* fMask;
    /** IS verbose? */
    bool fVerbose;
    bool fDebug;
  };
}

#endif
//____________________________________________________________________
//
//  EOF
//

