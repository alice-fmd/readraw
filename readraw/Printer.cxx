//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Printer.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:31:10 2006
    @brief   Implementation of printer base visitor 
*/
#include "Printer.h"
#include "Event.h"
#include "Header.h"
#include "EventId.h"
#include "AttributePattern.h"
#include "Equipment.h"
#include "CommonHeader.h"
#include <iostream>
#include <iomanip>
#include <cmath>

//____________________________________________________________________
std::ostream& operator<<(std::ostream& o, const ReadRaw::Spacer& space)
{
  if (space.N() <= 0) return o;
  return o << std::setw(space.N()) << " ";
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const Event& event) 
{
  static unsigned long i = 0;
  if (fMask & kEvent) fOut << fSpacer << "Event: (" << i 
			   << " in file)" << std::endl;
  i++;
  SpacerGuard g(fSpacer);
  if (!Visitor::Visit(event)) return false;
  return true;
}

namespace { 
  const char*
  Type2String(ReadRaw::uint32_t type) 
  {
    switch (type) { 
    case 1:	return "Start of run";		// kStartOfRun,
    case 3:	return "Start of run files";	// kStartOfRunFiles,
    case 10:	return "Start of data";		// kStartOfData,
    case 5:	return "Start of burst";	// kStartOfBurst,
    case 6:	return "End of burst";		// kEndOfBurst,
    case 7:	return "Physics event";		// kPhysicsEvent,
    case 8:	return "Calibration event";	// kCalibrationEvent,
    case 12:	return "System Software Trigger event";	
      // kSystemSoftwareTriggerEvent,
    case 13:	return "Detector Software Trigger event";	
      // kDetectorSoftwareTriggerEvent,
    case 11:	return "End of data";		// kEndOfData,
    case 2:	return "End of run";		// kEndOfRun,
    case 4:	return "End of run files";	// kEndOfRunFiles,
    };
    return "Unknown";
  }      
}
//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const Header& header) 
{
  if (fMask & kHeader) 
    fOut << fSpacer << "Header: (Version 0x" 
	 << std::hex << header.Version() << std::dec << ")" 
	 << std::endl;
  SpacerGuard g(fSpacer);
  time_t     t = header.Timestamp();
  struct tm* l = localtime(&t);
  if (fMask & kHeader) 
    fOut << fSpacer << "Size:      " << header.Size() << "\n"
	 << fSpacer << "Magic:     0x" << std::hex << header.Magic() 
	 << std::dec << "\n"
	 << fSpacer << "Head size: " << header.HeadSize() << "\n" 
	 << fSpacer << "Version:   " << header.Version() << "\n"
	 << fSpacer << "Type:      " << Type2String(header.Type()) << " - " 
	 << header.Type() << "\n" 
	 << fSpacer << "Run #:     " << header.RunNb() << "\n"
	 << fSpacer << "LDC:       " << header.LdcId() << "\n"
	 << fSpacer << "GDC:       " << header.GdcId() << "\n"
	 << fSpacer << "Timestamp: " << asctime(l) << std::flush;
  
  if (!Visitor::Visit(header)) return false;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const EventId& id) 
{
  if (fMask & kEventId)
    fOut << fSpacer << "Event ID:" << std::endl;
  SpacerGuard g(fSpacer);
  if (fMask & kEventId)
    fOut << fSpacer << "Bunch X'ing/# in run: " 
	 << id.BunchCrossing() << "/\t" << id.NumberInRun() << "\n" 
	 << fSpacer << "Orbit #/Burst #:      " << id.Orbit() << "/\t" 
	 << id.BurstNumber() << "\n" 
	 << fSpacer << "Period/# in burst:    " << id.Period() << "/\t" 
	 << id.NumberInBurst() << std::endl;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const TriggerPattern& triggers) 
{
  if (fMask & kTrigger)
    fOut << fSpacer << "Triggers:    " << triggers << std::endl;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const DetectorPattern& detectors) 
{
  if (fMask & kDetector)
    fOut << fSpacer << "Detectors:   " << detectors << std::endl;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const AttributePattern& attributes) 
{
  if (fMask & kAttribute)
    fOut << fSpacer << "System:      " << attributes.System() << "\n"
	 << fSpacer << "User:        " << attributes.User() << std::endl;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const Equipment& equipment)
{
  if (fMask & kEquipment)
    fOut << fSpacer << "Equipment:" << std::endl;
  SpacerGuard g(fSpacer);
  if (fMask & kEquipment)
    fOut << fSpacer << "Size:         " << equipment.Size() << "\n" 
	 << fSpacer << "Type:         " << equipment.Type() << "\n" 
	 << fSpacer << "Id            " << equipment.Id() << "\n" 
	 << fSpacer << "Element size: " << equipment.ElementSize() 
	 << std::endl;
  if (!Visitor::Visit(equipment)) return false;
  return true;  
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const CommonHeader& header) 
{
  if (fMask & kCommon)
    fOut << fSpacer << "Common Header:" << std::endl;
  SpacerGuard g(fSpacer);
  if (fMask & kCommon)
    fOut << fSpacer << "Length:            " << header.Length() << "\n" 
	 << fSpacer << "Event ID 1:        " << header.EventId1() << "\n" 
	 << fSpacer << "L1 Message:        " << header.L1Message() << "\n" 
	 << fSpacer << "Version:           " << header.Version() << "\n" 
	 << fSpacer << "Event ID 2:        " << header.EventId2() << "\n" 
	 << fSpacer << "Sub-detectors:     " << header.SubDetectors() << "\n" 
	 << fSpacer << "Attributes:        " << header.Attributes() << "\n" 
	 << fSpacer << "Mini event ID:     " << header.MiniEvent() << "\n" 
	 << fSpacer << "Status & error:    0x" << std::hex 
	 << header.StatusError() << "\n" 
	 << fSpacer << "Triggers:          0x" << header.TriggerClass() <<"\n" 
	 << fSpacer << "Region of Intrest: 0x" << header.RegionOfIntrest() 
	 << std::dec << std::endl;
  if (!Visitor::Visit(header)) return false;
  return true;
}

//____________________________________________________________________
bool
ReadRaw::Printer::Visit(const uint32_t* data, const uint32_t  size) 
{
  if ((fMask & kData) == 0)  return true;

  size_t n = size_t(log10(size))+2;
  fOut << fSpacer << "Data:" << std::endl;
  SpacerGuard g(fSpacer);
  for (size_t i = 0; i < size; i++) {
    if (i != 0 && i % 5 == 0) fOut << std::endl;
    if (i % 5 == 0) 
      fOut << fSpacer << std::setw(n) << i << ":";
    fOut << std::hex << std::setfill('0') << "  0x" << std::setw(8) 
	 << data[i] << std::dec << std::setfill(' ');
  }
  fOut << "\n" << fSpacer << "End of Data" << std::endl;
  return true;
}

//____________________________________________________________________
//
// EOF
//

  





  
