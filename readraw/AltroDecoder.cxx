//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AltroDecoder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:26:08 2006
    @brief   Implementation of ALTRO decoder
*/
#include "AltroDecoder.h"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <iterator>

//____________________________________________________________________
const ReadRaw::uint40_t 
ReadRaw::AltroDecoder::fgkTrailerMask = ((ReadRaw::uint40_t(0x2aaa) << 26) + 
					 (ReadRaw::uint40_t(0xa) << 12));

//____________________________________________________________________
void
ReadRaw::AltroDecoder::Clear()
{
  for (ChannelMap::iterator i = fMap.begin(); i != fMap.end(); ++i) {
    if (!i->second) continue;
    // std::cout << "Deleting channel at " << i->first << std::endl;
    delete  i->second;
    i->second = 0;
  }
  fMap.clear();
}


//____________________________________________________________________
size_t
ReadRaw::AltroDecoder::DecodeTrailer(const uint32_t* data, 
				     const uint32_t  size, 
				     const uint8_t   vers)
{
  fTrailer.clear();
  size_t nw40   = 0;
  size_t lsize  = sizeof(uint32_t);
  size_t remain = 0;
  if (fDebug) std::cout << "Last word: 0x" 
			<< std::hex << std::setfill('0') 
			<< std::setw(8) << data[size-1] 
			<< std::setfill(' ') << std::dec 
			<< std::endl;
  size_t   nw_trailer = 0;
  uint16_t rcu_id     = 0;
  uint8_t  rcu_vers   = 0;
  if ((data[size-1] & 0xFFFF0000) == 0xAAAA0000) {
    // This is version 2 of the data format.  
    rcu_id     = (data[size-1] >> 7) & 0x1FF;
    nw_trailer = (data[size-1] >> 0) & 0x07F;
    nw40       = (data[size-nw_trailer]);
  }
  else if (((data[size-1] >> 30) & 0x3) == 0x3) { 
    // This is version 3 of the data format 
    if (fDebug) 
      std::cout << "Saw version 3 of the data format" << std::endl;
    nw_trailer = (data[size-1] >> 0)  & 0x7F;
    rcu_id     = (data[size-1] >> 7)  & 0x1FF;
    rcu_vers   = (data[size-1] >> 16) & 0xff;
    nw40       = (data[size-nw_trailer]) & 0x3FFFFFF;
    if (fDebug) { 
      std::cout << "Words of trailer: " << std::endl;
      for (size_t i = 0; i < nw_trailer; i++) { 
	size_t j = size-nw_trailer+i;
	std::cout << "\t"     << std::setw(2) << i 
		  << std::hex << std::setfill('0')  << ": 0x" 
		  << " -> 0x" << std::setw(8) << j 
		  << "/0x"    << std::setw(8) << size 
		  << ": "     << std::setw(8) << data[j] 
		  << std::dec << std::setfill(' ') << std::endl;
      }
    }
    if (rcu_vers != vers) 
      std::cerr << "Hmm - mismatch between format version " 
		<< vers << " and RCU FW version " << rcu_vers 
		<< std::endl;
  }
  else {
    // Figure out where our data stops. 
    if (fDebug) std::cout << "Old RCU data format" << std::endl;
    nw40   = size * lsize / 5;
    remain = size * lsize * 8 - nw40 * 40;

    // If we got less than a full word in the remainder, then the last
    // 40 it word is a partial extra word, so we disregard that. 
    if (remain < 32) nw40--;
  }
    
  if (fDebug) 
    std::cout << "Trailer information: \n"
	      << " # trailer words:  " << nw_trailer << "\n"
	      << " # of 40bit words: " << nw40 
	      << std::hex << std::setfill('0')
	      << "(0x" << nw40 << ")\n" 
	      << " RCU Id:           0x" << std::setw(3) << rcu_id 
	      << std::endl;
  
  if (nw_trailer > 0) { 
    for (size_t i = size-nw_trailer; i < size-2; i++) { 
      uint8_t  type  = (data[i] >> 30) & 0x3;
      uint8_t  param = (data[i] >> 26) & 0x1F;
      uint32_t value = (data[i] >>  0) & 0x1FFFFFF;
      fTrailer[param] = value;
      if (fDebug) 
	std::cout << " Type 0x"       << std::setw(1) << unsigned(type)
		  << " Parameter 0x"  << std::setw(2) << unsigned(param)
		  << ":  0x"          << std::setw(7) << value << std::endl;
    }
  }
  if (fDebug) std::cout << std::dec << std::setfill(' ') << std::flush;
  return nw40;
}

//____________________________________________________________________
bool
ReadRaw::AltroDecoder::DecodeSodEod(const uint32_t* data, 
				    const uint32_t  size, 
				    const uint8_t   vers)
{
  size_t nw24 = DecodeTrailer(data, size, vers);
  if (fDebug) 
    std::cout << "Got " << std::dec << std::setfill(' ') 
	      << std::setw(8) << nw24 
	      << " 24bit words" << std::endl;

  // Re-pack into bytes.
  uint32_t last  = 0;
  bool     isData = false;
  for (size_t i = 0; i < nw24; i++) 
    DecodeResult(data[i], last, isData, true);
  return true;
}

//____________________________________________________________________
bool
ReadRaw::AltroDecoder::DecodeEvent(const uint32_t* data,  
				   const uint32_t  size, 
				   const uint8_t   vers) 
{
  Clear();
  size_t nw40   = DecodeTrailer(data, size, vers);

  if (fDebug) 
    std::cout << "Got " << std::dec << std::setfill(' ') 
	      << std::setw(8) << nw40 
	      << " 40bit words" << std::endl;

  if (vers < 2) return DecodeEventV1(data, nw40);
  return DecodeEventV2(data, nw40);
}

//____________________________________________________________________
bool
ReadRaw::AltroDecoder::DecodeEventV1(const uint32_t* data,  
				     size_t          nw40)
{
  size_t lsize  = sizeof(uint32_t);

  // Re-pack into 10bit words
  size_t asize = nw40 * 4;
  size_t cur   = 0;
  size_t off   = 0;
  std::vector<uint16_t> adata(asize); 

  // Read the old format - 10bit and 40bit words are packed tight in
  // 32bit words, and the data is back-linked. 
  for (size_t i = 0; i < asize; i++) {
    uint16_t t = 0;
    size_t rem = lsize * 8 - off;
    if (rem == 0) {
      cur++;
      t = (data[cur] >> off) & 0x3ff;
      off = 10;
    }
    else if (rem < 10) {
      size_t low = (lsize * 8 - (10 - rem));
      t = (((data[cur] >> off)) + 
	   ((data[cur+1] << low) >> (low - rem)));
      cur++;
      off = 10 - rem;
    }
    else {
      t   =  (data[cur] >> off) & 0x3ff;
      off += 10;
    }
    if (t > 0x3ff) {
      std::cerr << "Bad value at " << i << ": 0x" << std::hex << t 
		<< std::dec << std::endl;
      return false;
    }
    adata[i] = t;
  }
    
  if (fVerbose) DumpData(adata, std::cout);
  
  // Decode  channel data 
  int ret;
  while ((ret = DecodeChannelV1(adata)) > 0);
  if (ret < 0) return false;
  
  return true;
}

//____________________________________________________________________
bool
ReadRaw::AltroDecoder::DecodeEventV2(const uint32_t* data,  
				     size_t          nw40)
{
  // 10bit words a packed in groups of 3 into 32bit words, and the
  // data is forward linked.  Also, if bit 30 is asserted in a 32
  // bit word, it means we have a channel trailer (or header)
  const uint32_t* ptr  = data;
  
  while (ptr < (data + nw40)) { 
    if (DecodeChannelV2(ptr) < 0) { 
      std::cerr << "Failed to decode a channel" << std::endl;
      return false;
    }
    if (fDebug)
      std::cout << "Pointer at " << ((void*)ptr) 
		<< " [" << ((void*)(data + nw40)) << "]" << std::endl;
  }

  return true;
}

#define CH_OUT(B,A,C,L) "  Board: "   << std::setw(2) << (B)	\
  << "  Chip: "    << std::setw(1) << (A)			\
  << "  Channel: " << std::setw(2) << (C)			\
  << "  Last: "    << std::setw(4) << (L)  

//____________________________________________________________________
int
ReadRaw::AltroDecoder::DecodeChannelV1(std::vector<uint16_t>& data)
{
  if (data.size() < 5) return 0;
  uint40_t trailer = ((uint40_t(data[data.size()-1]) << 30) +
		      (uint40_t(data[data.size()-2]) << 20) + 
		      (uint40_t(data[data.size()-3]) << 10) + 
		      (uint40_t(data[data.size()-4]) <<  0));
  if (!IsTrailer(trailer)) {
    std::cerr << "Error: Word # " << data.size()-4 << " 0x" 
	      << std::hex << std::setw(10) << std::setfill('0') 
	      << trailer << std::dec << std::setfill(' ')
	      << " does not fit a trailer"  << std::endl;
    // DumpData(data, std::cerr);
    return -1;
  }
  uint16_t channel = (trailer >>  0) & 0x00f;
  uint16_t chip    = (trailer >>  4) & 0x007;
  uint16_t board   = (trailer >>  7) & 0x01f;
  uint16_t last    = (trailer >> 16) & 0x3ff;
  if (fDebug) 
    std::cout << "Trailer word # " << std::setw(4) << data.size() - 4 
	      << CH_OUT(board,chip,channel,last) << std::endl;
  size_t nfill   = (last % 4 == 0 ? 0 : 4 - last % 4);
  if (fVerbose) { 
    size_t off  = data.size()-last-nfill-4;
    DumpData(&(data[off]), data.size()-off, std::cout);
  }
  
  data.pop_back();
  data.pop_back();
  data.pop_back();
  data.pop_back();  
  
  for (size_t i = 0; i < nfill; i++) {
    uint16_t fill = data[data.size()-1];
    if (fill != 0x2aa) {
      std::cerr << "Warning: Invalid fill 0x" <<  std::hex << fill << std::dec 
		<< " at " << i << " for " << CH_OUT(board, chip,channel, last) 
		<< std::endl;
      // DumpData(data);
      // return -1;
    }
    data.pop_back();
    if (fDebug) 
      std::cout << "Fill Word # " << std::setw(4) << i << " 0x" 
		<< std::hex << fill << std::dec << std::endl;
  }
  size_t        index = (channel + (chip << 4) + (board << 7));
  int           rem   = 0;
  int           t     = 0;
  // std::cout << "Adding channel at " << index << std::endl;
  AltroChannel* ch    = fMap[index] = new AltroChannel;
  ch->fBoard          = board;
  ch->fChip           = chip;
  ch->fChannel        = channel;
  
  for (size_t i = 0; i < last; i++) {
    if (data.size() <= 0) {
      std::cerr << "Error: Too many words read: " << i << " for " 
		<< CH_OUT(board,chip,channel,last) << std::endl;
      return -1;
    }
    if (rem == 0) {
      // New bunch 
      rem = data[data.size()-1]-2;
      if (rem < 1) { 
	std::cerr << "Error: Bunch length is short: " << rem << " at " 
		  << last-i-1 << " for " << CH_OUT(board,chip,channel,last)
		  << std::endl;
	// DumpData(data, std::cerr);
	return -1;
      }

      data.pop_back();
      int oldt = t;
      t = data[data.size()-1];
      if (oldt != 0 && oldt < t) 
	std::cerr << "Warning: Old time " << std::setw(4) << oldt 
		  << " before now " << std::setw(4) << t << " @ " 
		  << last-i << " for " << CH_OUT(board,chip,channel,last)
		  << std::endl;
      data.pop_back();
      i += 2;
      if (fDebug) 
	std::cout << "Bunch words # " << std::setw(4) << last-i+1
		  << "  Remain: " << std::setw(4) << rem 
		  << "  T:      " << std::setw(4) << t << std::endl;
      t++;
    }
    t--;
    if (t < 0) { 
      std::cerr << "Warning: Negative time (" << t << ") @ " << last-i 
		<< " for " << CH_OUT(board,chip,channel,last) << std::endl;
      // DumpData(&(data[data.size()-rem]), rem, std::cerr);
      // Increase counter by length 
      for (int j = 0; j < rem && data.size() > 0; ++j) data.pop_back();
      i += rem;
      rem = 0;
      continue;
    }
    if (t > ch->fMaxT) ch->fMaxT = t;
    if (t < ch->fMinT) ch->fMinT = t;
    ch->fData[t] = data[data.size()-1];
    rem--;
    data.pop_back();
#if 1
    if (fDebug) 
      std::cout << "ADC word # " << std::setw(4) << last-i-1
		<< "   T: "   << std::setw(4) << t 
		<< "   ADC: " << std::setfill('0') 
		<< std::setw(4) << ch->fData[t] << std::dec
		<< std::setfill(' ') << std::endl;
#endif
  }
  if (data.size() <= 0) return 0;
  return 1;
}

#define SHOW_32(O,W) \
  do {			\
    if (!fVerbose) break; \
    (O) << std::hex << std::setfill('0') << "0x" << std::setw(8) << W << " " \
	<< " 0x" << std::setw(2) << ((W >> 30) & 0x3)			     \
	<< " 0x" << std::setw(3) << ((W >> 20) & 0x3FF)			     \
	<< " 0x" << std::setw(3) << ((W >> 10) & 0x3FF)			     \
	<< " 0x" << std::setw(3) << ((W >>  0) & 0x3FF)			     \
	<< std::dec << std::setfill('0') << std::endl; } while (false)

//____________________________________________________________________
int
ReadRaw::AltroDecoder::DecodeChannelV2(const uint32_t*& ptr)
{
  uint32_t        word = 0;
  do { 
    word = *(ptr++);
    SHOW_32(std::cout, word);
  } while ((word >> 30) != 1);

  
  bool     bad     = (word >> 29) & 0x1;
  uint16_t cnt     = (word >> 16) & 0x3FF;
  uint16_t hwa     = (word >>  0) & 0xFFF;
  uint16_t nwd     = (cnt + 2) / 3;
  uint16_t channel = (hwa >>  0) & 0x00f;
  uint16_t chip    = (hwa >>  4) & 0x007;
  uint16_t board   = (hwa >>  7) & 0x01f;
  size_t   index   = (channel + (chip << 4) + (board << 7));
  if (fDebug) 
    std::cout << "Trailer word " << CH_OUT(board,chip,channel,cnt) 
	      << " " << nwd << " 32bit words" << std::endl;
  if (bad) 
    std::cerr << "Channel " << CH_OUT(board,chip,channel,cnt) << " has errors"
	      << std::endl;

  // Make a channel object 
  AltroChannel* ch    = fMap[index] = new AltroChannel;
  ch->fBoard          = board;
  ch->fChip           = chip;
  ch->fChannel        = channel;

  enum { length, time, data } state = length;
  int      rem   = 0;
  int      t     = 0;

  for (uint16_t iw = 0; iw < nwd; iw++) { 
    word = *ptr++;
    SHOW_32(std::cout, word);
    if ((word >> 30) != 0x0) {
      std::cerr << "Unexpected end of channel payload" << std::endl;
      return -1;
    }
    for (int16_t it = 2; it >= 0; it--) { 
      uint16_t w10 = (word >> it * 10) & 0x3FF;
      size_t   i   = cnt - (3 * iw - it);
      switch (state) { 
      case length: t = 0;   rem = w10 - 1; state = time; break;
      case time:   
	t = w10; 
	rem--;         
	state = data;
	if (fDebug) 
	  std::cout << "Bunch words # " << std::setw(4) << i
		    << "  Remain: "     << std::setw(4) << rem 
		    << "  T:      "     << std::setw(4) << t << std::endl;
	break;
      case data:   
	t--;
	rem--;
	if (t < 0) { 
	  rem   = 0;
	  state = length;
	  continue;
	}
	state = (rem == 0 ? length : data);
	if (t > ch->fMaxT) ch->fMaxT = t;
	if (t < ch->fMinT) ch->fMinT = t;
	ch->fData[t] = w10;

	if (fDebug) 
	  std::cout << "ADC word # " << std::setw(4) << i
		    << "   T: "      << std::setw(4) << t 
		    << "   ADC: "    << std::setfill('0') << std::setw(4) 
		    << ch->fData[t]  << std::setfill(' ') << std::endl;
	break;
      }
    }
  }
  return 1;
}


//____________________________________________________________________
#define SHOW_40W(O,X)					    \
  do {							    \
    (O) << "\t0x" << std::setw(10) << X << " ";		    \
    if (!fVerbose) { (O) << std::endl; break; }		    \
    unsigned char* b = (unsigned char*)&X;                  \
    for (int j = 4; j >= 0; j--)                            \
      (O) << " 0x" << std::setw(2) << unsigned(b[j]);	    \
    (O) << std::endl; } while(false)
  

//____________________________________________________________________
void
ReadRaw::AltroDecoder::DumpData(std::vector<uint16_t>& data,
				std::ostream& out)
{
  DumpData(&(data[0]), data.size(), out);
}
//____________________________________________________________________
void
ReadRaw::AltroDecoder::DumpData(uint16_t* data, uint16_t n, 
				std::ostream& out)
{
  uint40_t bla = 0;
  out << "# of bytes : " << n << "\n" << std::hex << std::setfill('0');
  for (size_t i = 0; i < n; i++) {
    if (i != 0 && i % 4 == 0) SHOW_40W(out,bla);
    if (i % 4 == 0) {
      bla = 0;
      out << std::setw(5) << std::dec << std::setfill(' ') << i 
	  << ":"  << std::hex << std::setfill('0');
    }
    bla += (uint40_t(data[i]) << ((i % 4) * 10));
    out << " 0x" << std::setw(3) << data[i] << std::flush;
  }
  SHOW_40W(out,bla);
  out << std::setfill(' ') << std::dec << std::endl;
}

//____________________________________________________________________
bool
ReadRaw::AltroDecoder::DecodeResult(uint32_t word, uint32_t& last, 
				    bool& isData, bool print)
{

  if (fDebug) 
    std::cout << "    0x" 
	      << std::hex << std::setfill('0') 
	      << std::setw(6) << (word & 0xffffff)
	      << std::dec << std::setfill(' ') << std::endl;
  uint32_t what         = (isData ? last : word);
  uint32_t address      = (0xffffff & what);
  uint32_t type         = ((address >> 21) & 0x0f);
  uint32_t error        = ((address >> 20) & 0x01);
  uint32_t bcast        = ((address >> 18) & 0x01);
  uint32_t bc_not_altro = ((address >> 17) & 0x01);
  uint32_t board        = ((address >> 12) & 0x1f);
  uint32_t instruction  = address & (bc_not_altro ? 0xfff : 0x1f);
  uint32_t chip         = (bc_not_altro ? 0 : ((address >> 9) & 0x7));
  uint32_t channel      = (bc_not_altro ? 0 : ((address >> 5) & 0x5));

  bool        readData = false;
  bool        nextData = false;
  switch (type) { 
  case 0x0: readData = true;        // Fec read;
  case 0x1:                         // Fec cmd;
  case 0x2:                         // Fec write
    nextData = true;                // Next call will be on data 
    break;
  case 0x4:                         // Loop
  case 0x5:                         // Wait
    break;
  case 0x6:                         // End sequence 
  case 0x7:                         // End ,e,ory 
    break;
  }

  if (print && !isData) {
    std::cout << std::hex << std::setfill('0') << "0x" << std::setw(6) 
	      << address << " ";
    switch (type) {
    case 0x0: std::cout << "FEC Read:     ";  break;
    case 0x1: std::cout << "FEC Command:  ";  break;
    case 0x2: std::cout << "FEC Write:    ";  break;
    case 0x4: std::cout << "Loop:         ";  break;
    case 0x5: std::cout << "Wait:         ";  break;
    case 0x6: std::cout << "End sequence: ";  break;
    case 0x7: std::cout << "End memory:   ";  break;
    default:  std::cout << "Unknown:      ";  break;
    }
    std::cout << (error ? "failure " : "success ");
    if (bcast) 
      // 9+4=13
      std::cout << "Broadcast    ";
    else {
      if (bc_not_altro) 
	// 2+2+9=13
	std::cout << "0x" << std::setw(2) << board << "         ";
      else 
	// 2+2+3+1+3+1+1=13
	std::cout << "0x" << std::setw(2) << board 
		  << "/0x" << chip << "/0x" << channel << " ";
    }
    std::cout << "0x" << std::setw(3) << instruction 
	      << std::dec << std::setfill(' ') << std::endl;
  } 
  if (!isData) { 
    isData = nextData;
    last   = word;
    return true;
  }

  // Here we do the data word 
  last   = 0;
  isData = false;

  if (!print) return true;

  uint32_t data = (word & 0xFFFFFF);
  std::cout << std::hex << std::setfill('0') << "0x" << std::setw(6)
	    << data << " ";
  switch (instruction) {
  case 0x01: std::cout << "First ADC T                 "; break;
  case 0x02: std::cout << "I  3.3 V                    "; break;
  case 0x03: std::cout << "I  2.5 V altro digital      "; break;
  case 0x04: std::cout << "I  2.5 V altro analog       "; break;
  case 0x05: std::cout << "I  2.5 V VA                 "; break;
  case 0x06: std::cout << "First ADC T                 "; break;
  case 0x07: std::cout << "I  3.3 V                    "; break;
  case 0x08: std::cout << "I  2.5 V altro digital      "; break;
  case 0x09: std::cout << "I  2.5 V altro analog       "; break;
  case 0x0A: std::cout << "I  2.5 V VA                 "; break;
  case 0x2D: std::cout << "Second ADC T                "; break;
  case 0x2E: std::cout << "I  1.5 V VA                 "; break;
  case 0x2F: std::cout << "I -2.0 V                    "; break;
  case 0x30: std::cout << "I -2.0 V VA                 "; break;
  case 0x31: std::cout << "   2.5 V Digital driver     "; break;
  case 0x32: std::cout << "Second ADC T                "; break;
  case 0x33: std::cout << "I  1.5 V VA                 "; break;
  case 0x34: std::cout << "I -2.0 V                    "; break;
  case 0x35: std::cout << "I -2.0 V VA                 "; break;
  case 0x36: std::cout << "   2.5 V Digital driver     "; break;
  case 0x37: std::cout << "Third ADC T                 "; break;
  case 0x38: std::cout << "Temperature sens. 1         "; break;
  case 0x39: std::cout << "Temperature sens. 2         "; break;
  case 0x3A: std::cout << "U  2.5 altro digital (m)    "; break;
  case 0x3B: std::cout << "U  2.5 altro analog (m)     "; break;
  case 0x3C: std::cout << "Third ADC T                 "; break;
  case 0x3D: std::cout << "Temperature sens. 1         "; break;
  case 0x3E: std::cout << "Temperature sens. 2         "; break;
  case 0x3F: std::cout << "U  2.5 altro digital (m)    "; break;
  case 0x40: std::cout << "U  2.5 altro analog (m)     "; break;
  case 0x41: std::cout << "Forth ADC T                 "; break;
  case 0x42: std::cout << "U  2.5 VA (m)               "; break;
  case 0x43: std::cout << "U  1.5 VA (m)               "; break;
  case 0x44: std::cout << "U -2.0 VA (m)               "; break;
  case 0x45: std::cout << "U -2.0 (m)                  "; break;
  case 0x46: std::cout << "Forth ADC T                 "; break;
  case 0x47: std::cout << "U  2.5 VA (m)               "; break;
  case 0x48: std::cout << "U  1.5 VA (m)               "; break;
  case 0x49: std::cout << "U -2.0 VA (m)               "; break;
  case 0x4A: std::cout << "U -2.0 (m)                  "; break;
    // Counters                                    
  case 0x0B: std::cout << "L1 trigger CouNTer          "; break;
  case 0x0C: std::cout << "L2 trigger CouNTer          "; break;
  case 0x0D: std::cout << "Sampling CLK CouNTer        "; break;
  case 0x0E: std::cout << "DSTB CouNTer                "; break;
    // Test mode                                   
  case 0x0F: std::cout << "Test mode word              "; break;
  case 0x10: std::cout << "Undersampling ratio.        "; break;
    // Configuration and status                    
  case 0x11: std::cout << "Config/Status Register 0    "; break;
  case 0x12: std::cout << "Config/Status Register 1    "; break;
  case 0x13: std::cout << "Config/Status Register 2    "; break;
  case 0x14: std::cout << "Config/Status Register 3    "; break;
  case 0x15: std::cout << "Free                        "; break;
    // Comands:                                    
  case 0x16: std::cout << "Latch L1, L2, SCLK Counters "; break;
  case 0x17: std::cout << "Clear counters              "; break;
  case 0x18: std::cout << "Clear CSR1                  "; break;
  case 0x19: std::cout << "rstb ALTROs                 "; break;
  case 0x1A: std::cout << "rstb BC                     "; break;
  case 0x1B: std::cout << "Start conversion            "; break;
  case 0x1C: std::cout << "Scan event length           "; break;
  case 0x1D: std::cout << "Read event length           "; break;
  case 0x1E: std::cout << "Start test mode             "; break;
  case 0x1F: std::cout << "Read acquisition memory     "; break;
    // FMD                                         
  case 0x20: std::cout << "FMDD status                 "; break;
  case 0x21: std::cout << "L0 counters                 "; break;
  case 0x22: std::cout << "FMD: Wait to hold           "; break;
  case 0x23: std::cout << "FMD: L1 timeout             "; break;
  case 0x24: std::cout << "FMD: L2 timeout             "; break;
  case 0x25: std::cout << "FMD: Shift clk              "; break;
  case 0x26: std::cout << "FMD: Strips                 "; break;
  case 0x27: std::cout << "FMD: Cal pulse              "; break;
  case 0x28: std::cout << "FMD: Shape bias             "; break;
  case 0x29: std::cout << "FMD: Shape ref              "; break;
  case 0x2A: std::cout << "FMD: Preamp ref             "; break;
  case 0x2B: std::cout << "FMD: Sample clk             "; break;
  case 0x2C: std::cout << "FMD: Commands               "; break;
  case 0x4B: std::cout << "FMD: Cal events             "; break;
  default:   std::cout << "Unknown                     "; break;
  }
  std::cout << "0x" << std::setw(6) << data 
	    << std::dec << std::setfill(' ') << std::endl;

  return true;
}

//____________________________________________________________________
//
// EOF
//
