//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Monitor.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:30:35 2006
    @brief   Implmentation of on-line monitor class
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Monitor.h"
#include <monitor.h>
#include <event.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace 
{
  bool have_monitor = false;
}

//____________________________________________________________________
ReadRaw::Monitor::Monitor() 
{
  if (have_monitor) 
    throw std::runtime_error("only one monitor per program!");
  have_monitor = true;
  fIsEOD       = true;
  int ret      = monitorDeclareMp(const_cast<char*>("ReadRaw::Monitor"));
  CheckReturn(ret);
}

//____________________________________________________________________
ReadRaw::Monitor::~Monitor() 
{
  int ret = monitorLogout();
  CheckReturn(ret);
}
  
//____________________________________________________________________
ReadRaw::Monitor::Monitor(const Monitor&) 
{
  throw std::runtime_error("cannot copy a monitor");
}

//____________________________________________________________________
ReadRaw::Monitor&
ReadRaw::Monitor::operator=(const Monitor&) 
{
  throw std::runtime_error("cannot assign to a monitor");
}


//____________________________________________________________________
bool
ReadRaw::Monitor::SetInput(const char* source)
{
  fIsEOD  = false;
  int ret = monitorSetDataSource(const_cast<char*>(source));
  return CheckReturn(ret);
}

//____________________________________________________________________
void
ReadRaw::Monitor::SetMask(const Mask& m) 
{
  int ret = monitorDeclareTable(const_cast<char**>(m.AsTable()));
  CheckReturn(ret);
}

//____________________________________________________________________
void
ReadRaw::Monitor::SetMask(const AttributeMask& m) 
{
  int ret = monitorDeclareTableWithAttributes(const_cast<char**>(m.AsTable()));
  CheckReturn(ret);
}

//____________________________________________________________________
void
ReadRaw::Monitor::Flush()
{
  int ret = monitorFlushEvents();
  CheckReturn(ret);
}

//____________________________________________________________________
bool
ReadRaw::Monitor::IsEOD()
{
  return fIsEOD;
}

//____________________________________________________________________
void
ReadRaw::Monitor::SetWait(bool on)
{
  int ret = 0;
  if (on) ret = monitorSetWait();
  else    ret = monitorSetNowait();
  CheckReturn(ret);
}

//____________________________________________________________________
bool
ReadRaw::Monitor::CheckReturn(int ret) 
{
  if (ret == 0) return true;
  if (ret != MON_ERR_EOF) {
    std::cerr  << "Error: 0x" << std::hex << ret << ": " << std::dec 
	       << monitorDecodeError(ret) << std::endl;
  }
  fIsEOD = true;
  return false;
}

//____________________________________________________________________
bool
ReadRaw::Monitor::GetNextEvent(ReadRaw::Event& e)
{
  void* ptr;
  int ret = monitorGetEventDynamic(&ptr);
  if (!CheckReturn(ret))  { return false; }
  if (!ptr)               return false;
  eventStruct*            pEvent  =  reinterpret_cast<eventStruct*>(ptr);

  // How, let our Event class deal with the actual decoding 
  e.Clear();
  e.Set((uint32_t*)ptr);
  if (TEST_SYSTEM_ATTRIBUTE(pEvent->eventHeader.eventTypeAttribute,
			    ATTR_EVENT_SWAPPED)) {
    // Flag that the bytes are flipped
    e.Head().Attributes().FlipSystem(66);
  }

  return true;
}

//____________________________________________________________________
//
// EOF
//
