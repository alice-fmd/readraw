//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    EventId.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 20:51:04 2006
    @brief   Implementation of EventId class 
*/
#include "EventId.h"
#include <iostream>

//____________________________________________________________________
ReadRaw::EventId::EventId(const EventId& other) 
{ 
  fData = other.fData;
}
//____________________________________________________________________
bool
ReadRaw::EventId::Set(uint32_t* data)
{
  fData = data;
  return true;
}

//____________________________________________________________________
ReadRaw::EventId::EventId& 
ReadRaw::EventId::operator=(const EventId& other) 
{
  fData[0] = other.fData[0];
  fData[1] = other.fData[1];
  return *this;
}
//____________________________________________________________________
bool 
ReadRaw::EventId::operator==(const EventId& other) const 
{
  return fData[0] == other.fData[0] && fData[1] == other.fData[1];
}
//____________________________________________________________________
bool 
ReadRaw::EventId::operator>(const EventId& other) const 
{
  return (fData[0] > other.fData[0] || 
	  (fData[0] == other.fData[0] && fData[1] > other.fData[1]));
  
}
//____________________________________________________________________
bool 
ReadRaw::EventId::operator<(const EventId& other) const 
{
  return (fData[0] < other.fData[0] || 
	  (fData[0]==other.fData[0] && (fData[1]<other.fData[1])));
}

//____________________________________________________________________
//
// EOF
//
