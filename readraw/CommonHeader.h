// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    CommonHeader.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:17:03 2006
    @brief   Declaration of common header class 
*/
#ifndef READRAW_COMMONHEADER_H
#define READRAW_COMMONHEADER_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif

namespace ReadRaw
{

  //==================================================================
  /** @struct CommonHeader 
      The common data header 
      @ingroup Data 
   */
  struct CommonHeader 
  {
    /** Constructor  */
    CommonHeader() : fData(0) {}
    
    /** @{ 
	@name Error and status bits */
    /** @return true if trigger overlap bit is set */ 
    bool IsTriggerOverlap()     const { return StatusError() & (1 << 0); }
    /** @return true if trigger was missing */ 
    bool IsTriggerMissing()     const { return StatusError() & (1 << 1); }
    /** @return true data has the wrong polarity */ 
    bool WrongDataParity()      const { return StatusError() & (1 << 2); }
    /** @return true control data has the wrong polarity */ 
    bool WrongControlParity()   const { return StatusError() & (1 << 3); }
    /** @return true if there's no trigger information */ 
    bool NoTriggerInformation() const { return StatusError() & (1 << 4); }
    /** @return true if there's an error in the Front-end-electronics */ 
    bool FeeError()             const { return StatusError() & (1 << 5); }
    /** @return true if HTL made a decision */
    bool IsHltDecision()        const { return StatusError() & (1 << 6); }
    /** @return true if this contains HTL payload */
    bool HasHltPayload()        const { return StatusError() & (1 << 7); }
    /** @return true if this contains DDG payload */
    bool HasDdgPayload()        const { return StatusError() & (1 << 8); }
    /** @return true if there was an L1 time violation */
    bool L1TimeViolation()      const { return StatusError() & (1 << 9); }
    /** @return true if there was an L2 time violation */
    bool L2TimeViolation()      const { return StatusError() & (1 << 10); }
    /** @return true if there was an L2 time violation */
    bool IsPrepulseError()      const { return StatusError() & (1 << 11); }
    /** @return true if there was an L2 time violation */
    bool IsError()              const { return StatusError() & (1 << 12); }
    /** @} */

    /** @return the block length */ 
    uint32_t Length()       const { return fData[0]; }
    /** @return Event id # 1 */
    uint32_t EventId1()     const { return fData[1] & 0xfff; }
    /** @return L1 trigger message */ 
    uint32_t L1Message()    const { return (fData[1] >> 14) & 0x3ff; }
    /** @return Version of common header */ 
    uint32_t Version()      const { return (fData[1] >> 24) & 0xff; }
    /** @return Event id # 2 */
    uint32_t EventId2()     const { return (fData[2] & 0xffffff); }
    /** @return bit pattern of sub-detectors */ 
    uint32_t SubDetectors() const { return (fData[3] & 0xffffff); }
    /** @return Attributes */
    uint32_t Attributes()   const { return (fData[3] >> 24) & 0xff; }
    /** @return Mini event Id */
    uint32_t MiniEvent()    const { return fData[4] & 0xfff; }
    /** @return Status and error bits */ 
    uint32_t StatusError()  const { return (fData[4] >> 12) & 0xffff;  }
    /** @return Trigger class bits */ 
    uint64_t TriggerClass() const; 
    /** @return Region of intrest bits */ 
    uint64_t RegionOfIntrest() const;
    /** @return Size of common header in 32 bit words */
    uint32_t Size() const { return Version() < 1 ? 7 : 8; }
    /** Set from data words 
	@param data The data 
	@param check Whether we should check the header */
    bool Set(uint32_t* data, bool check=true);
    /** clear it */
    void Clear() { fData = 0; }
  protected:
    /** Pointer to the data */ 
    uint32_t* fData;
  };
  //__________________________________________________________________
  inline uint64_t
  CommonHeader::TriggerClass() const 
  { 
    return (uint64_t(fData[5]) << 18) | (fData[6] & 0x3ffff); 
  }
  //__________________________________________________________________
  inline uint64_t
  CommonHeader::RegionOfIntrest() const
  { 
    return (Version() < 1 ? 0 : 
	    ((fData[6] << 28) & 0xf) | (uint64_t(fData[7]) << 4));
  }
}
#endif
//
// EOF
//
