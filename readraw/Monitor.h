// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Monitor.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:57:35 2006
    @brief   Declaration of monitor class
*/
#ifndef READRAW_MONITOR_H
#define READRAW_MONITOR_H
#ifndef READRAW_READER_H
# include <readraw/Reader.h>
#endif

namespace ReadRaw 
{
  /** @struct Monitor 
      @ingroup Reader 
      An online monitor 
   */
  struct Monitor : public Reader
  {
    /** Destructor */
    virtual ~Monitor(); 
    /** Set the input source 
	@param input Source */ 
    virtual bool SetInput(const char* input);
    /** Set the mask to use 
	@param m Mask to use  */
    virtual void SetMask(const Mask& m);
    /** Set the mask to use 
	@param m Mask to use  */
    virtual void SetMask(const AttributeMask& m);
    /** Set whether to wait or not */
    virtual void SetWait(bool on=true);
    /** Flush stored events */
    virtual void Flush();
    /** @return Return @a true if no more data can be read */
    virtual bool IsEOD();
    /** Get the next event. 
	@return If an error occured, or the event was not selected by
	the mask, then retur 0.  If an error occured, IsEOD will
	return true. */
    virtual bool GetNextEvent(Event& e);
  protected: 
    friend class Reader;
    /** Constructor */
    Monitor();
    /** Whether we can read more */
    bool fIsEOD;
    /** Check return value 
	@param ret Return value
	@return  @c true on success */
    virtual bool CheckReturn(int ret);
  private:
    /** Copy constructor 
	@param other Object to copy from */
    Monitor(const Monitor& other);
    /** 
     * Assignment operator 
     * 
     * @param other Object to assign from 
     * @return Reference to this object. 
     */
    Monitor& operator=(const Monitor& other);
  };
}

#endif
//
// EOF
//

  
    
