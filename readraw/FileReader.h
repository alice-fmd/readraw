// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FileReader.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Jun 22 19:55:25 2006
    @brief   
*/
#ifndef READRAW_FILEREADER_H
#define READRAW_FILEREADER_H
#ifndef READRAW_READER_H
# include <readraw/Reader.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
namespace ReadRaw
{
  //==================================================================
  /** Concrete class to read events from a file 
      @ingroup Reader
   */
  struct FileReader : public Reader
  {
    /** Destructor */
    virtual ~FileReader();
    /** Add an input file 
	@param input Input source stream */
    virtual bool AddInput(const char* input);
    /** Set the input file name */ 
    virtual bool SetInput(const char* filename);
    /** Set input file names */
    virtual bool SetInput(const std::vector<std::string>& files);
    /** @return Return @a true if no more data can be read */
    virtual bool IsEOD();
    /** Get the next event. 
	@return If an error occured, or the event was not selected by
	the mask, then retur 0.  If an error occured, IsEOD will
	return true. */
    virtual bool GetNextEvent(Event& e);
    /** Get an estimate of the number of events */ 
    long Events() const;
  protected:
    friend class Reader;
    /** Constructor  */
    FileReader();
    /** Copy constructor 
	@param other Object to copy from */
    FileReader(const FileReader& other);
    /** Assignmet operator 
	@param other Object to copy from
	@return Reference to this object */
    FileReader& operator=(const FileReader& other);
    /** open next input file */
    virtual bool OpenNext();
    /** Get next 32bit word from file */
    virtual bool GetNextWord(uint32_t& w, bool swap=false);
    /** Input stream */
    std::istream* fInput;
    /** Current input number in multi-part */
    long fCurrent;
    /** Vector of input files */
    std::vector<std::string> fFiles;
    /** Total size in bytes */
    uint64_t fTotal;
    /** Total size in bytes */
    uint64_t fRead;
    /** Events read */ 
    uint64_t fEvents;
  };
}

#endif
//____________________________________________________________________
//
//  EOF
//

