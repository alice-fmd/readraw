//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    FileReader.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:32:06 2006
    @brief   Implementation of file reader class 
*/
#include "FileReader.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <cerrno>
#include <cstring>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <climits>
#include <limits>
#ifndef ULLONG_MAX
/** @def ULLONG_MAX
    @brief Macro that expands to maximum value of an @c unsigned @c
    long @c long. 
 */
# define ULLONG_MAX std::numeric_limits<uint64_t>::max()
#endif

//====================================================================
ReadRaw::FileReader::FileReader()
  : fInput(0), fCurrent(0)
{
  fTotal  = 0;
  fRead   = 0;
  fEvents = 0;
}

//====================================================================
ReadRaw::FileReader::FileReader(const FileReader& other)
  : Reader(other), 
    fInput(other.fInput), 
    fCurrent(other.fCurrent)
{}

//====================================================================
ReadRaw::FileReader&
ReadRaw::FileReader::operator=(const FileReader& other)
{
  fInput       = other.fInput;
  fCurrent     = other.fCurrent;
  return *this;
}

//____________________________________________________________________
ReadRaw::FileReader::~FileReader()
{
  if (!fInput) return;
  delete fInput;
}

//____________________________________________________________________
bool
ReadRaw::FileReader::AddInput(const char* filename)
{
  try {
    if (access(filename, F_OK) != 0) return false;
    if (access(filename, R_OK) != 0) throw errno;
  
    struct stat buf;
    int ret = stat(filename, &buf);
    if (ret < 0) throw errno;

    if (fTotal > ULLONG_MAX - buf.st_size)
      std::cerr << "Adding " << buf.st_size << " to " 
		<< fTotal << " would overflow" << std::endl;
    else 
      fTotal += buf.st_size;
    fFiles.push_back(std::string(filename));
    if (fVerbose) 
      std::cout << "Adding file " << filename << " (" 
		<< (buf.st_size / 1024 / 1024) << "MB/"
		<< (fTotal / 1024 / 1024) << "MB)" << std::endl;
  }
  catch (int e) {
    std::cerr << "Couldn't add source " << filename << ": " 
	      << strerror(errno) << std::endl;
    return false;
  }
  return true;
}

//____________________________________________________________________
bool
ReadRaw::FileReader::SetInput(const std::vector<std::string>& files)
{
  if (fInput) {
    delete fInput;
    fInput = 0;
  }
  fFiles.clear();
  fTotal  = 0;
  fRead   = 0;
  fEvents = 0;
  
  for (size_t i = 0; i < files.size(); i++) 
    if (!AddInput(files[i].c_str())) return false;
  fCurrent = -1;
  return OpenNext();
}

  
  
//____________________________________________________________________
bool
ReadRaw::FileReader::SetInput(const char* filename) 
{
  if (fInput) {
    delete fInput;
    fInput = 0;
  }
  fFiles.clear();
  fTotal  = 0;
  fRead   = 0;
  fEvents = 0;
  
  std::string name(filename);
  if (name.empty())       return false;
  if (!AddInput(filename)) return false;
  
  if (name.find(".000") != std::string::npos) {
    size_t pos = name.find(".000");
    size_t i = 1;
    while (true) {
      std::string fname(name);
      std::stringstream s;
      s << "." << std::setfill('0') << std::setw(3) << i;
      fname.replace(pos, 4, s.str());
      if (!AddInput(fname.c_str())) break;
      i++;
    }
  }
  fCurrent = -1;
  return OpenNext();
}

//____________________________________________________________________
long
ReadRaw::FileReader::Events() const
{
  if (fRead <= 0 || fEvents <= 0) return -1;
  return (fTotal) / (fRead / fEvents);
}


//____________________________________________________________________
bool
ReadRaw::FileReader::OpenNext()
{
  fCurrent++;
  if (size_t(fCurrent) >= fFiles.size()) return false;
  std::string&  filename = fFiles[fCurrent];
  fInput = new std::ifstream(filename.c_str());
  if (!fInput||!*fInput) {
    std::cerr << "Failed to open source " << filename << ": " 
	      << strerror(errno) << std::endl;
    return false;
  }
  if (fVerbose) 
    std::cout << "Opened file # " << std::setw(8) << fCurrent 
	      << "/" << std::setw(8) << fFiles.size() << ": " 
	      << filename << std::endl;
  return true;
}
//____________________________________________________________________
bool
ReadRaw::FileReader::IsEOD()
{
  if (!fInput)        return true;
  if (fInput->eof())  return !OpenNext();
  if (fInput->fail()) return true;
  return false;
}

//____________________________________________________________________
bool
ReadRaw::FileReader::GetNextEvent(ReadRaw::Event& e) 
{
  if (IsEOD()) return false;
  uint32_t size, magic, headSize, tmp;
  fEvents++;
  
  // Read pre-ample
  if (!GetNextWord(size))     return false;
  if (!GetNextWord(magic))    return false;
  if (!GetNextWord(headSize)) return false;
  bool    needSwap = (magic == 0xFE5A1EDA);
  if (needSwap) {
    size     = SwapBytes(size);
    magic    = SwapBytes(magic);
    headSize = SwapBytes(headSize);
  }
  if (magic    != 0xDA1E5AFE           || 
      size     <= 3 * sizeof(uint32_t) ||
      headSize <= 3 * sizeof(uint32_t) || 
      size     <  headSize) {
    std::stringstream s;
    s << "Bad preample: Magic=0x" << std::hex << magic << " (0x" 
      << 0xDA1E5AFE << std::dec << " size=" << size 
      << " headSize=" << headSize;
    throw std::runtime_error(s.str().c_str());
    return false;
  }

  // std::vector<unsigned long> event(size);
  uint32_t* event = new uint32_t[size];
  event[0] = size;
  event[1] = magic;
  event[2] = headSize;


  size_t headWords = headSize / sizeof(uint32_t);
  size_t nWords    = size / sizeof(uint32_t);
  // Read rest of header 
  for (size_t i = 3; i < headWords; i++) {
    if (!GetNextWord(tmp, needSwap)) return false;
    event[i] = tmp;
  }
  
  // Now read the rest of data 
  for (size_t i = headWords; i < nWords; i++) {
    if (!GetNextWord(tmp, needSwap)) return false;
    event[i] = tmp;
  }   
  // Hmm.  Perhaps this should be moved before the above loop?
  if (fMask && !(fMask->AsBitPattern() & (1 << event[4]))) return false;

  // How, let our Event class deal with the actual decoding 
  e.Clear();
  e.Set(&(event[0]));
  // Event* ret = new Event(&(event[0]));
  if (needSwap) {
    // Flag that the bytes are flipped
    e.Head().Attributes().FlipSystem(66);
  }
  return true;
}
  

//____________________________________________________________________
bool
ReadRaw::FileReader::GetNextWord(uint32_t& w, bool needSwap) 
{
  if (!fInput || fInput->eof()) return false;  
  if (!*fInput) throw std::runtime_error("bad stream");
  const size_t n = sizeof(w);
  char b[n];
  fInput->read(b, n);
  fRead += n;
  uint32_t* pw = (uint32_t*)b;
  w = *pw;
  if (needSwap) w = SwapBytes(w);
  if (fDebug) { 
    std::cout << "Read " << n << " bytes:" << std::hex << std::setfill('0');
    for (int i = 0; i < n; i++) std::cout <<  " 0x" << std::setw(2) 
					  << int(b[i] & 0xff);
    std::cout << " -> " << std::setw(n*2) << w  
	      << " (" << (needSwap ? "swapped" : "straight")  << ")"
	      << std::dec << std::setfill(' ') << std::endl;
  }
  return true;
}

//____________________________________________________________________
//
// EOF
//  
  

  

