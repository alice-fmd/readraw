//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Header.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:07:57 2006
    @brief   Implementation of Header class 
*/
#include "Header.h"
#include <iostream>
#include <sstream>
#include <stdexcept>

//____________________________________________________________________
ReadRaw::Header::~Header()
{
  Clear();
  for (EquipmentMap::iterator i = fCache.begin(); i != fCache.end(); ++i) {
    delete (*i).second;
    // i->second = 0;
    // std::cout << std::endl;
  }
}

//____________________________________________________________________
bool
ReadRaw::Header::Set(uint32_t* data) 
{
  bool status = true;
  fData = data;
  if (!(status = fId.Set(&(fData[6])))) 
    std::cout << "Header: Failed to set event ID" << std::endl;
  if (!(status = fTriggerPattern.Set(&(fData[8]), Version()))) 
    std::cout << "Header: Failed to set trigger pattern" << std::endl;
  else if (!fTriggerPattern.IsOk()) {
    // throw std::runtime_error("Invalid trigger pattern");
    std::cerr << "Header: Invalid trigger pattern" << std::endl;
    status = false;
  }
  if (!(status = fDetectorPattern.Set(&(fData[10]), Version())))
    std::cout << "Header: Failed to set detector pattern" << std::endl;
  else if (!fDetectorPattern.IsOk()) {
    // throw std::runtime_error("Invalid detector pattern");
    std::cerr << "Header: Invalid detector pattern" << std::endl;
    status = false;
  }
  if (!(status = fAttributePattern.Set(&(fData[11]), Version())))
    std::cout << "Header: Failed to set attribute pattern" << std::endl;
  // if (!fAttributePattern.System().IsOk()) 
  //  throw std::runtime_error("Invalid system pattern");

  const size_t    nbytes   = sizeof(uint32_t);
  const size_t    dataSize = (Size() - HeadSize());
  uint32_t        read     = 0;
  uint32_t*       ptr      = &(fData[HeadSize() / nbytes]);
  while (read < dataSize) {
    uint32_t               id = ptr[2];
    Equipment*             e  = 0;
    EquipmentMap::iterator i  = fCache.find(id);
    if (i != fCache.end()) e  = i->second;
    if (!e)                e  = fCache[id] = new Equipment;
    fEquipment[id]            = e;    
    e->Clear();
    if (!(status = e->Set(ptr))) 
      std::cerr << "Header: Failed to set equipment" << std::endl;
    ptr                      += e->Size() / nbytes;
    uint32_t r               =  e->Size();
    read = read + r;
  }
  if (read != dataSize) {
    std::stringstream s;
    s << "Header: Read header and equipment doesn't add up to " 
      << Size() << " but " << read + 68;
    std::cerr << s.str() << std::endl;
    // throw std::runtime_error(s.str().c_str());
  }
  return status;
}

//____________________________________________________________________
void
ReadRaw::Header::Clear()
{
  fData = 0;
  fId.Clear();
  fTriggerPattern.Clear();
  fDetectorPattern.Clear();
  fAttributePattern.Clear();
  fEquipment.clear();
}

//____________________________________________________________________
bool
ReadRaw::Header::IsSwapped() const 
{
  return (Magic() == 0xFE5A1EDA);
}

//____________________________________________________________________
//
// EOF
//
