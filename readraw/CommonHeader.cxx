//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    CommonHeader.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:18:08 2006
    @brief   Implementation of CommonHeader class 
*/
#include "CommonHeader.h"
#include <iostream>
#include <cmath>

//____________________________________________________________________
bool
ReadRaw::CommonHeader::Set(uint32_t* data, bool verify) 
{
  fData = data;

  if (!verify) return true;
  unsigned int word[] = {   1,    2,   4,    6 };
  unsigned int off[]  = {  12,   24,  28,   18 };
  unsigned int mask[] = { 0x3, 0xff, 0xf, 0xff };
  bool         status = true;
  for (unsigned int i = 0; i < 4; i++) {
    unsigned check = (data[word[i]] >> off[i]) & mask[i];
    if (check != 0x0) {
      std::cerr << "In common data header, word " << word[i] << ", bits " 
		<< off[i] << " to " << off[i] + unsigned(log2(mask[1])-1) 
		<< " not null, but 0x" << std::hex << check 
		<< std::dec << std::endl;
      status = false;
    }
  }
  return status;
}
  
//____________________________________________________________________
//
// EOF
//
