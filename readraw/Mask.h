// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Mask.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 01:23:27 2006
    @brief   Declaration of masks 
*/
#ifndef READRAW_MASK_H
#define READRAW_MASK_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif 

namespace ReadRaw
{
  /** @struct Mask 
      Event mask for reader 
      @ingroup Reader
  */
  struct Mask 
  {
    /** Constructor */
    Mask();
    /** Copy constructor */
    Mask(const Mask& o);
    /** Destructor */
    virtual ~Mask() { ClearTable(); }
    /** Assignement operator */
    Mask& operator=(const Mask& m);

    /** Types of events */ 
    enum Type {
      /** All events */
      kAllEvents,
      /** Start of run (1) */
      kStartOfRun,
      /** Start of run files (3) */
      kStartOfRunFiles,
      /** Start of data (10) */
      kStartOfData,
      /** Start of burst (5) */
      kStartOfBurst,
      /** End of burst (6) */
      kEndOfBurst,
      /** Physics event (7) */
      kPhysicsEvent,
      /** Calibration event (8) */
      kCalibrationEvent,
      /** System Software Trigger event (12) */
      kSystemSoftwareTriggerEvent,
      /** Detector Software Trigger event (13) */
      kDetectorSoftwareTriggerEvent,
      /** End of data (11) */
      kEndOfData,
      /** End of run (2) */
      kEndOfRun,
      /** End of run files (4) */
      kEndOfRunFiles,
      /** Event format error */
      kEventFormatError
    };
  
    /** Flag that says how many events to get for a specific mask. */ 
    enum Flag {
      /** all events of this type are monitored (100%) */
      kAll,
      /** a sample of the events of this type is monitored */
      kSome,
      /** no events of this type are monitored */
      kNone
    };

    /** add an event type to mask 
	@param type Type of event 
	@param flag What to do 
	@param a    Bit pattern of attributes */
    virtual void AddToMask(Type type, Flag flag, uint32_t a=0);
    /** add an event type to mask 
	@param type Type of event */
    virtual void RemoveFromMask(Type type);
    /** Return mask as a bit pattern */ 
    uint32_t AsBitPattern() const;
    /** Return mask as a string table */ 
    virtual const char** AsTable() const;
    /** Print the mask */
    void Print() const;
    
  protected:
    /** Our mask of types */ 
    std::vector<uint32_t> fFlags;
    /** Our mask */
    mutable std::vector<const char*> fTable;
    /** Static array of types */
    static const char* fgkTypes[];
    /** Static array of flags */
    static const char* fgkFlags[];
    /** Clear table */
    virtual void ClearTable();
    
  };

  //____________________________________________________________________
  /** An event mask with attributes */
  struct AttributeMask : public Mask
  {
    /** Constructor */
    AttributeMask();
    /** Constructor */
    AttributeMask(const AttributeMask& m);
    /** Constructor */
    AttributeMask& operator=(const AttributeMask& m);
    /** Destructor */
    virtual ~AttributeMask() {}
    /** add an event type with attributes to mask 
	@param type Type of event 
	@param flag What to do
	@param attr Bit pattern of attributes */
    void AddToMask(Type type, Flag flag, uint32_t attr);
    /** add an event type to mask 
	@param type Type of event */
    virtual void RemoveFromMask(Type type);
    /** Return mask as a string table */ 
    virtual const char** AsTable() const;
    /** Print the mask */
    void Print() const;
  protected:
    /** Our mask of types */ 
    std::vector<uint32_t> fAttributes;
    /** Clear table */
    virtual void ClearTable();
  };
}

#endif
//____________________________________________________________________
//
// EOF
//

