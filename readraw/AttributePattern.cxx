//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AttributePattern.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:28:16 2006
    @brief   Implmenation of Attribute Pattern
*/
#include "AttributePattern.h"

//____________________________________________________________________
ReadRaw::AttributePattern::AttributePattern(uint32_t* data)
{
  Set(data, 0);
}

//____________________________________________________________________
ReadRaw::AttributePattern::AttributePattern(const AttributePattern& o)  
  : fUser(o.fUser),
    fSystem(o.fSystem)
{}

//____________________________________________________________________
bool
ReadRaw::AttributePattern::Set(uint32_t* data, uint32_t ver) 
{
  bool status = true;
  if (!(status = fUser.Set(&(data[0]), ver))) 
    std::cerr << "Failed to set user attributes" << std::endl;
  if (!(status = fSystem.Set(&(data[2]), ver))) 
    std::cerr << "Failed to set system attributes" << std::endl;
  return status;
}

//____________________________________________________________________
void
ReadRaw::AttributePattern::Clear()
{
  fUser.Clear();
  fSystem.Clear();
}

//____________________________________________________________________
ReadRaw::AttributePattern&
ReadRaw::AttributePattern::operator=(const AttributePattern& o) 
{ 
  fSystem = o.fSystem;
  fUser   = o.fUser;
  return *this;
} 

//____________________________________________________________________
//
// EOF
//


