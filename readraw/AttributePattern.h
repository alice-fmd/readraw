// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AttributePattern.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 21:06:21 2006
    @brief   Declaration of attribute pattern
*/
#ifndef READRAW_ATTRIBUTEPATTERN_H
#define READRAW_ATTRIBUTEPATTERN_H
#ifndef READRAW_PATTERN_H
# include <readraw/Pattern.h>
#endif

namespace ReadRaw
{  
  //__________________________________________________________________
  /** @struct AttributePattern 
      Pattern of attributes  
      @ingroup Data 
  */
  struct AttributePattern 
  {
    /** Type of system bit patterns */
    struct SystemPattern : public Pattern 
    {
      /** Constructor */
      SystemPattern() : Pattern(0, 0) {}
      /** Destructor */
      virtual ~SystemPattern() {}
      /** @return Number of 32bit words */
      size_t N()    const { return 1; }
      /** @return Minimum valid bit */
      size_t Min()  const { return 1; }
      /** @return Maximum valid bit */
      size_t Max()  const { return 95 - 64; }
      /**  @return @c true if the pattern is valid */
      bool   IsOk() const 
      {
	return (fData[1] & 0xC00000FF) != 0; 
      }
    };
  
    /** Type of user bit patterns */
    struct UserPattern : public Pattern 
    {
      /** Constructor */
      UserPattern() : Pattern(0, 0) {}
      /** Destructor */
      virtual ~UserPattern() {}
      /** @return Number of 32bit words */
      size_t N()    const { return 2; }
      /** @return Minimum valid bit */
      size_t Min()  const { return 1; }
      /** @return Maximum valid bit */
      size_t Max()  const { return 63; }
      /**  @return @c true if the pattern is valid */
      bool   IsOk() const { return true; }
    };

    /** Constructor */
    AttributePattern() {}
    /** Copy constructor */
    AttributePattern(const AttributePattern& o);
    /** Constructor 
	@param data 3 words of data */
    AttributePattern(uint32_t* data);
    /** Assignment operator 
	@param o Object to copy from
	@return reference to this object */
    AttributePattern& operator=(const AttributePattern& o);
    /** @{ 
	@name System attributes */
    /** @return Get system bit patterns  */
    SystemPattern& System()    { return fSystem; }
    /** @return Get system bit patterns  */
    const SystemPattern& System() const   { return fSystem; }
    /** @param b Set system bit */
    void SetSystem(size_t b)   { fSystem.Set(b-63); }
    /** @param b Clear system bit */
    void ClearSystem(size_t b) { fSystem.Clear(b-63); }
    /** @param b Flip system bit */
    void FlipSystem(size_t b)  { fSystem.Flip(b-63); }
    /** Check if bit @a b is set 
	@param b Bit to test
	@return @c true if @a b is set. */
    bool HasSystem(size_t b) const { return fSystem.Has(b-64); }
    /** Reset bits */
    void ZeroSystem()          { fSystem.Zero(); }
    /** @return @c true if the system bits is ok */
    bool IsOk()                const { return fSystem.IsOk(); } 
    /** @return @c true if Start bit is set */
    bool IsStart()             const { return HasSystem(64); }
    /** @return @c true if End bit is set */
    bool IsEnd()               const { return HasSystem(65); }
    /** @return @c true if Event data is swapped */
    bool IsEventSwapped()      const { return HasSystem(66); }
    /** @return @c true if Event data is paged */
    bool IsEventPaged()        const { return HasSystem(67); }
    /** @return @c true if Event is a super-event */
    bool IsSuperEvent()        const { return HasSystem(68); }
    /** @return @c true if we in collider mode */
    bool HasOrbitBunch()       const { return HasSystem(69); }
    /** @return @c true if we keep pages */
    bool DoKeepPages()         const { return HasSystem(70); }
    /** @return @c true if its an HLT decision */
    bool IsHltDecision()       const { return HasSystem(71); }
    /** @return @c true if event data is truncated */
    bool IsEventDataTruncated()const { return HasSystem(94); }
    /** @return @c true if there's an errord */
    bool IsEventError()        const { return HasSystem(95); }
    /** @} */

    /** @{ 
	@name User attributes */
    /** @return Get user bit patterns  */
    UserPattern& User()      { return fUser; }
    /** @return Get user bit patterns  */
    const UserPattern& User() const { return fUser; }
    /** @param b Set user bit */
    void SetUser(size_t b)   { fUser.Set(b); }
    /** @param b Clear user bit */
    void ClearUser(size_t b) { fUser.Clear(b); }
    /** @param b Flip user bit */
    void FlipUser(size_t b)  { fUser.Flip(b); }
    /** Check if bit @a b is set 
	@param b Bit to test
	@return @c true if @a b is set. */
    bool HasUser(size_t b)   { return fUser.Has(b); }
    /** Reset bits */
    void ZeroUser()          { fUser.Zero(); }
    /** @} */

    /** Set from data words 
	@param dat DAta words 
	@param ver Version number of event. */ 
    bool Set(uint32_t* dat, uint32_t ver);
    /** Clear the attributes  */
    void Clear();
  protected:
    /** User bit patterns */
    UserPattern   fUser;
    /** System bit patterns */
    SystemPattern fSystem;
  };
}
#endif
//
// EOF
// 
