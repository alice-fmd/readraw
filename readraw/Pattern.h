// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Pattern.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 20:59:22 2006
    @brief   Declaration and implementation of Pattern class
    template. 
*/
#ifndef READRAW_PATTERN_H
#define READRAW_PATTERN_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef __CASSERT__
# include <cassert>
#endif
#ifndef __CSTDDEF__
# include <cstddef>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace ReadRaw
{
  //==================================================================
  /** @struct Pattern 
      @ingroup Data 
      A class template for various kinds of bit-patterns 
  */
  struct Pattern
  {
    /** constructor */
    Pattern(uint32_t version=0) : fData(0), fVersion(version) { }
    /** Constructor 
	@param data The data 
	@param version Event format version */
    Pattern(uint32_t* data, uint32_t version) :fData(data),fVersion(version) {}
    /** Copy constructor */
    Pattern(const Pattern& o) : fData(o.fData), fVersion(o.fVersion) {}
    /** Destructor */
    virtual ~Pattern() {}
    /** Assignment operator */
    Pattern& operator=(const Pattern& o);
    /** Check 
	@return @c true if the pattern is OK */
    virtual bool IsOk() const { return true; }
    /** Get minimum bit 
	@return the minimum bit number */
    virtual size_t Min() const { return 0; }
    /** Get maximum bit 
	@return the maximum bit number */
    virtual size_t Max() const { return 0; }
    /** Get Number of words 
	@return the number of 32bit words in this pattern */
    virtual size_t N() const { return 0; }
    /** Is the pattern valid? */
    bool IsValid() const { return (fData[0] & Validator()) != 0; }
    /** Validate pattern */
    void Validate() { fData[0]   |= Validator(); }
    /** Invalidate the patteren */ 
    void InValidate() { fData[0] &= (0xffffffff ^ Validator()); }
    /** Set bit @a i */
    void Set(size_t i)   { fData[ToWord(i)] |=  ToBit(i); }
    /** Clear bit @a i */
    void Clear(size_t i) { fData[ToWord(i)] &= ~ToBit(i); }
    /** Flip bit @a i */
    void Flip(size_t i)  { fData[ToWord(i)] ^=  ToBit(i); }
    /** Check bit @a i */
    bool Has(size_t i) const  { return (fData[ToWord(i)] & ToBit(i)) != 0; }
    /** Zero pattern */
    void Zero() { for (size_t i = 0; i < N(); i++) fData[i] = 0; }
    /** Get the @a i word */
    uint32_t Word(size_t i) const { return (i >= N() ? 0 : fData[i]); }
    /** Assign from words 
	@param dat Data words 
	@param ver Version of event */ 
    bool Set(uint32_t* dat, uint32_t ver) 
    { 
      fData    = dat; 
      fVersion = ver;
      return true;
    }
    /** Clear the pattern  */
    void Clear() { fData = 0; fVersion = 0; }
  protected:
    /** @return Mask to use for validation  */
    uint32_t Validator() const;
    /** Check that @a i is valid */
    void CheckBit(size_t i) const;
    /** Get bit corresponding to @a i */
    size_t ToBit(size_t i) const { return (1 << (i & 0x1F)); }
    /** Get word corresponding to @a i */
    size_t ToWord(size_t i) const { CheckBit(i); return (i >> 5); }
    /** The words */
    uint32_t* fData;
    /** Version of the header */ 
    uint32_t fVersion;
  };
  //__________________________________________________________________
  inline 
  void
  Pattern::CheckBit(size_t i) const
  {
    if (!fData) std::cout << "No data: " << fData << std::endl;
    assert(fData);
    if (i < Min()) std::cout << "i=" << i << " < " << Min() << std::endl;
    if (i > Max()) std::cout << "i=" << i << " < " << Max() << std::endl;
    assert(i >= Min());
    assert(i <= Max());
  }
  //__________________________________________________________________
  inline uint32_t
  Pattern::Validator() const
  { 
    return (fVersion < 0x00030009 ? 0x80000000 : 1); 
  } 
  //__________________________________________________________________
  inline Pattern&
  Pattern::operator=(const Pattern& o) 
  { 
    fVersion = o.fVersion;    
    for (size_t i=0; i<N(); i++) fData[i]=o.fData[i];
    return *this;
  }
}

//____________________________________________________________________
/** Output stream of a Pattern entry 
    @param o   Stream
    @param p   Pattern object. 
    @return @a o */
inline std::ostream& 
operator<<(std::ostream& o, const ReadRaw::Pattern& p)
{
  for (size_t j = p.Min(); j <= p.Max(); j++) o << (p.Has(j) ? '*' : '-');
  return o;
}
#endif
//____________________________________________________________________
//
// EOF
//
