// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    EventId.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 20:57:56 2006
    @brief   Declaration of EventId class 
*/
#ifndef READRAW_EVENTID_H
#define READRAW_EVENTID_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif

namespace ReadRaw 
{
  //==================================================================
  /** @struct EventId 
      Event identifier 

      <table>
        <tr><td width="28em"><tt>Period</tt></td>
	  <td width="4em"><tt>Orbit</tt></tr>
	<tr><td width="20em"><tt>Orbit</tt></td>
	  <td width="12em"><tt>Bunch crossing</tt></tr>
      </table>

      @ingroup Data
   */
  struct EventId 
  {
    /** Constructor */
    EventId() : fData(0) {}
    /** Copy constructor */
    EventId(const EventId& other);
    /** Assignment operator */
    EventId& operator=(const EventId& other);
    /** Destructor */
    virtual ~EventId() {}
    /** Comparison operator */
    bool operator==(const EventId& other) const;
    /** Comparison operator */
    bool operator>(const EventId& other) const;
    /** Comparison operator */
    bool operator<(const EventId& other) const;
    /** Comparison operator */
    bool operator>=(const EventId& other) const;
    /** Comparison operator */
    bool operator<=(const EventId& other) const;
    /** @return Get Bunch crossing number (only valid in collider mode) */
    virtual uint32_t BunchCrossing() const;
    /** @return Get orbit number (only valid in collider mode) */
    virtual uint32_t Orbit() const;
    /** @return Get period number (only valid in collider mode) */
    virtual uint32_t Period() const;
    /** Set Bunch crossing number (only valid in collider mode) 
	@param x Value */
    virtual void SetBunchCrossing(uint32_t x);
    /** Set orbit number (only valid in collider mode) 
	@param x Value */
    virtual void SetOrbit(uint32_t x);      
    /** Set period number (only valid in collider mode) 
	@param x Value */
    virtual void SetPeriod(uint32_t x);
    /** @return Get number in Run (only valid in fixed target mode) */
    virtual uint32_t NumberInRun() const;
    /** @return Get burst number (only valid in fixed target mode) */
    virtual uint32_t BurstNumber() const;
    /** @return Get number in burst (only valid in fixed target mode) */
    virtual uint32_t NumberInBurst() const; 
    /** Set number in run (only valid in fixed target mode) 
	@param x Value */
    virtual void SetNumberInRun(uint32_t x);
    /** Set burst number (only valid in fixed target mode) 
	@param x Value */
    virtual void SetBurstNumber(uint32_t x);
    /** Set number in burst (only valid in fixed target mode) 
	@param x Value */
    virtual void SetNumberInBurst(uint32_t x);
    /** Set from data words 
	@param data Data words */ 
    bool Set(uint32_t* data);
    /** Clear the structure. */
    void Clear() { fData = 0; }
  protected:
    /** Data */
    uint32_t* fData;
  };
  //__________________________________________________________________
  inline bool 
  EventId::operator>=(const EventId& other) const 
  { 
    return !(this->operator<(other)); 
  }
  //__________________________________________________________________
  inline bool 
  EventId::operator<=(const EventId& other) const 
  {
    return !(this->operator>(other));
  }
  //__________________________________________________________________
  inline uint32_t
  EventId::BunchCrossing() const 
  { 
    return (fData[1]&0x00000fff); 
  }
  //__________________________________________________________________
  inline uint32_t
  EventId::Orbit() const 
  { 
    return (((fData[0]) << 20 & 0xf00000) | ((fData[1] >> 12) & 0xfffff));
  }
  //__________________________________________________________________
  inline uint32_t
  EventId::Period() const 
  { 
    return (fData[0] >> 4) & 0x0fffffff; 
  }
  //__________________________________________________________________
  inline void 
  EventId::SetBunchCrossing(uint32_t x) 
  {
    fData[1] = (fData[1] & 0xfffff000) | (x & 0xfff);
  }
  //__________________________________________________________________
  inline void 
  EventId::SetOrbit(uint32_t x) 
  {
    fData[0] = (fData[0] & 0xfffffff0) | ((x & 0x00f00000) >> 20);
    fData[1] = (fData[1] & 0x00000fff) | ((x & 0x000fffff) << 12);
  }      
  //__________________________________________________________________
  inline void 
  EventId::SetPeriod(uint32_t x) 
  {
    fData[0] = (fData[0] & 0x0000000f) | ((x & 0xffffffff) << 4);
  }
  //__________________________________________________________________
  inline uint32_t 
  EventId::NumberInRun() const 
  { 
    return fData[0]; 
  }
  //__________________________________________________________________
  inline uint32_t 
  EventId::BurstNumber() const 
  { 
    return ((fData[1] >> 20) & 0x00000fff); 
  }
  //__________________________________________________________________
  inline uint32_t 
  EventId::NumberInBurst() const 
  { 
    return (fData[1]&0x000fffff); 
  } 
  //__________________________________________________________________
  inline void 
  EventId::SetNumberInRun(uint32_t x) 
  { 
    fData[0] = x; 
  }
  //__________________________________________________________________
  inline void 
  EventId::SetBurstNumber(uint32_t x) 
  { 
    fData[1] = (fData[1] & 0x000fffff) | ((x << 20) & 0xfff00000);
  }
  //__________________________________________________________________
  inline void 
  EventId::SetNumberInBurst(uint32_t x) 
  {
    fData[1] = (fData[1] & 0xfff00000) | (x & 0x000ffffff);
  }
}
#endif
//
// EOF
//
