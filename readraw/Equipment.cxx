//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Equipment.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:14:46 2006
    @brief   Implementation of Equipment header class
*/
#include "Equipment.h"
#include <iostream>
#include <iomanip>

namespace 
{
  // Equipment header is 7*32bit
  // Common data header is 8*32bit - or is it 7!?
  // size_t kDataOffset = 7 + 7;
}

//____________________________________________________________________
bool ReadRaw::Equipment::fVerify = true;

//____________________________________________________________________
ReadRaw::Equipment::Equipment()
  : fData(0), 
    fDataOffset(0), 
    fN(0), 
    fPayload(0)
{}

//____________________________________________________________________
void
ReadRaw::Equipment::Clear() 
{
  fData       = 0;
  fAttributes.Clear();
  fHeader.Clear();
  fDataOffset = 0;
  fN          = 0;
  fPayload    = 0;
}

//____________________________________________________________________
bool
ReadRaw::Equipment::Set(uint32_t* data)
{
  fData = data;
  bool status = true;
  if (!(status = fAttributes.Set(&(fData[3]), 0))) 
    std::cerr << "Failed to set attributes" << std::endl;
  if (!(status = fHeader.Set(&(fData[7]), fVerify))) 
    std::cerr << "Failed to set common data header" << std::endl;
  fDataOffset = 7 + fHeader.Size();
  fN          = fData[0] / sizeof(uint32_t) - fDataOffset;
  fPayload    = &(fData[fDataOffset]);
  return status;
}

//____________________________________________________________________
ReadRaw::Equipment::~Equipment()
{}

//____________________________________________________________________
//
// EOF
//
