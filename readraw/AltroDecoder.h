// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AltroDecoder.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jun 26 20:10:43 2006
    @brief   Declaration of decoder or ALTRO data 
*/
#ifndef READRAW_ALTRODECODER_H
#define READRAW_ALTRODECODER_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __MAP__
# include <map>
#endif
#include <iosfwd>

namespace ReadRaw 
{
  /** @defgroup ALTRO ALTRO specific classes
   */
  /** @struct AltroChannel 
      Class to store data for one ALTRO channel. 
      @ingroup ALTRO
   */
  struct AltroChannel 
  {
    /** 
     * Constructor 
     */
    AltroChannel() : 
      fData(1024), 
      fBoard(0), 
      fChip(0), 
      fChannel(0), 
      fMinT(1024), 
      fMaxT(0)
    {}
    /** Whether the data is valid */ 
    bool IsValid() const { return (fMaxT < fMinT); }
    /** ADC data */
    std::vector<uint16_t> fData;
    /** Board # */
    uint16_t fBoard;
    /** Chip # */
    uint16_t fChip;
    /** Channel # */
    uint16_t fChannel;
    /** Minimum time bin width data */
    uint16_t fMinT; 
    /** Maximum time bin width data */
    uint16_t fMaxT;
  };

  /** 
   * @struct AltroDecoder 
   *
   * Class to decode ALTRO formatted data 
   *
   * @ingroup ALTRO
   */
  struct AltroDecoder 
  {
    /** 
     * Constructor 
     */
    AltroDecoder(bool verbose=false, bool debug=false) 
      : fVerbose(verbose), fDebug(debug)
    {}
    /** 
     * Destructor 
     */
    virtual ~AltroDecoder() {}

    /** 
     * Clear internal array 
     */
    void Clear();
    /** 
     * Decode an event
     *
     * @param data Array of 32bit words 
     * @param size Number of 32bit words in @a data 
     * @param vers Format version
     *
     * @return @c true on success, @c false otherwise 
     */
    bool DecodeEvent(const uint32_t* data, 
		     const uint32_t  size,
		     const uint8_t   vers);
    /** 
     * Decode an SOD/EOD event
     *
     * @param data Array of 32bit words 
     * @param size Number of 32bit words in @a data 
     * @param vers Format version
     *
     * @return @c true on success, @c false otherwise 
     */
    bool DecodeSodEod(const uint32_t* data, 
		      const uint32_t  size,
		      const uint8_t   vers);
    /** 
     * Decode a channel 
     * 
     * @param data Array of 32bit words (each with 24bit valid)
     * 
     * @return @c true on success, @c false otherwise 
     */
    bool DecodeResult(uint32_t data, uint32_t& last, bool& isData, 
		      bool print=true);
    /** 
     * Set verbosity 
     *
     * @param verb IF true, be verbose 
     */
    void SetVerbose(bool verb=true) { fVerbose = verb; }
  protected:
    bool DecodeEventV1(const uint32_t* data, 
		       size_t          nw40);
    bool DecodeEventV2(const uint32_t* data, 
		       size_t          nw40);
    /** 
     * Decode a channel 
     *
     * @param data Array of 10bit words 
     *
     * @return negative value on error, 0 if there's no more data, 
     *         and 1 if there's more data. 
     */
    int DecodeChannelV1(std::vector<uint16_t>& data);
    /** 
     * Decode a channel 
     *
     * @param ptr Array of 32bit words 
     *
     * @return negative value on error, 0 if there's no more data, 
     *         and 1 if there's more data. 
     */
    int DecodeChannelV2(const uint32_t*& ptr);

    /** 
     * Decode a channel trailer
     * 
     * @param data Data array
     * @param size Size of @a data
     * @param vers Format version
     * 
     * @return Number of words read
     */    
    size_t DecodeTrailer(const uint32_t* data, 
			 const uint32_t  size, 
			 const uint8_t   vers);
    /** 
     * Test wether @a w matches a trailer 
     *
     * @param w 40 bit word to test 
     */
    bool IsTrailer(const uint40_t& w) const 
    { 
      return (w  & fgkTrailerMask) == fgkTrailerMask; 
    }
    /** 
     * Dump the data @a data to standard output. 
     *
     * @param data Data to show 
     * @param out  Output stream
     */
    void DumpData(std::vector<uint16_t>& data, std::ostream& out);
    /** 
     * Dump the data @a data to standard output. 
     *
     * @param data Data to show 
     * @param n    Number of words 
     * @param out  Ourput stream
     */
    void DumpData(uint16_t* data, uint16_t n, std::ostream& out);
    /** Trailer mask */ 
    static const uint40_t fgkTrailerMask;
    /** Map of channel - index is full channel address */
    typedef std::map<size_t,AltroChannel*> ChannelMap;
    /** Map of channel - index is full channel address */
    ChannelMap fMap;
    /** Parameters in the RCU trailer */ 
    typedef std::map<unsigned short, unsigned int> TrailerMap;
    /** Parameters in the RCU trailer */ 
    TrailerMap fTrailer;
    /** Whether to be verbose */
    bool fVerbose;
    /** Whether to be debug */
    bool fDebug;
    
    
  };
}

#endif 
//____________________________________________________________________
//
// EOF
//


  
