//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Mask.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:30:07 2006
    @brief   Implemenation of Mask class
*/
#include "Mask.h"
#include <iostream>
#include <iomanip>
#include <sstream>

//____________________________________________________________________
const char*
ReadRaw::Mask::fgkFlags[] =  {
  "all",		// kAll 
  "yes",		// kSome 
  "no",			// kNone
  0
};

//____________________________________________________________________
const char*
ReadRaw::Mask::fgkTypes[] = { 
  "ALL",  // kAllEvents           	    All events
  "SOR",  // kStartOfRun          	    Start of run
  "SORF", // kStartOfRunFiles     	    Start of run files
  "SOD",  // kStartOfData         	    Start of data
  "SOB",  // kStartOfBurst        	    Start of burst
  "EOB",  // kEndOfBurst          	    End of burst
  "PHY",  // kPhysicsEvent        	    Physics event
  "CAL",  // kCalibrationEvent    	    Calibration event
  "SST",  // kSystemSoftwareTriggerEvent    System Software Trigger event
  "DST",  // kDetectorSoftwareTriggerEvent  Detector Software Trigger event
  "EOD",  // kEndOfData           	    End of data
  "EOR",  // kEndOfRun            	    End of run
  "EORF", // kEndOfRunFiles       	    End of run files
  "FERR", // kEventFormatError    	    Event format error              
  0
};

//____________________________________________________________________
ReadRaw::Mask::Mask() 
  : fFlags(14)
{
  for (size_t i = 0; i < fFlags.size(); i++) fFlags[i] = kNone;
  ClearTable();
}

//____________________________________________________________________
ReadRaw::Mask::Mask(const Mask& m) 
  : fFlags(m.fFlags.size())
{
  for (size_t i = 0; i < fFlags.size(); i++) fFlags[i] = m.fFlags[i];
  ClearTable();
}

//____________________________________________________________________
ReadRaw::Mask&
ReadRaw::Mask::operator=(const Mask& m) 
{
  fFlags.resize(m.fFlags.size());
  for (size_t i = 0; i < fFlags.size(); i++) fFlags[i] = m.fFlags[i];
  ClearTable();
  return *this;
}

//____________________________________________________________________
void 
ReadRaw::Mask::ClearTable() 
{
  fTable.clear();
}

//____________________________________________________________________
void 
ReadRaw::Mask::AddToMask(Type type, Flag flag, uint32_t) 
{
  ClearTable();
  fFlags[type] = flag;
}

//____________________________________________________________________
void 
ReadRaw::Mask::RemoveFromMask(Type type)
{
  ClearTable();
  fFlags[type] = kNone;
}

//____________________________________________________________________
ReadRaw::uint32_t
ReadRaw::Mask::AsBitPattern() const 
{
  if (fFlags[0] != kNone) return 0xffffffff;
  unsigned int ret = 0;
  for (size_t i = 1; i < fFlags.size(); i++) 
    if (fFlags[i] != kNone) ret += (1 << (i+1));
  return ret;
}

//____________________________________________________________________
const char**
ReadRaw::Mask::AsTable() const
{
  if (fTable.empty()) {
    // size_t j = 0;
    for (size_t i = 0; i < fFlags.size(); i++) {
      fTable.push_back(fgkTypes[i]);
      fTable.push_back(fgkFlags[fFlags[i]]);
    }
    fTable.push_back((char*)0);
  }
  return &(fTable[0]);
}

//____________________________________________________________________
void 
ReadRaw::Mask::Print() const 
{
  std::cout << "Mask is:\n" 
	    << "  Bit pattern: 0x" << std::hex << std::setfill('0')
	    << std::setw(8) << AsBitPattern() << std::dec << std::setfill(' ')
	    << "\n  String table:" << std::endl;
  const char** table = AsTable();
  const char** p     = table;
  while (*p) {
    const char* type = (*p); p++;
    const char* flag = (*p); p++;
    std::cout << "    " << type << "=" << flag << " " << std::endl;
  }
}

//====================================================================
ReadRaw::AttributeMask::AttributeMask() 
  : Mask(), fAttributes(fFlags.size())
{}

//____________________________________________________________________
ReadRaw::AttributeMask::AttributeMask(const AttributeMask& m) 
  : Mask(m), fAttributes(m.fAttributes.size())
{
  for (size_t i = 0; i < fAttributes.size(); i++) 
    fAttributes[i] = m.fAttributes[i];
  ClearTable();
}

//____________________________________________________________________
ReadRaw::AttributeMask&
ReadRaw::AttributeMask::operator=(const AttributeMask& m) 
{
  fFlags.resize(m.fFlags.size());
  for (size_t i = 0; i < fFlags.size(); i++) fFlags[i] = m.fFlags[i];
  fAttributes.resize(m.fAttributes.size());
  for (size_t i = 0; i < fAttributes.size(); i++) 
    fAttributes[i] = m.fAttributes[i];
  ClearTable();
  return *this;
}

//____________________________________________________________________
void 
ReadRaw::AttributeMask::ClearTable() 
{
  size_t n = fTable.size();
  for (size_t i = 0; i < n / 3; i++) delete [] fTable[i * 3 + 2];
  fTable.clear();
}
  
//____________________________________________________________________
void 
ReadRaw::AttributeMask::AddToMask(Type type, Flag flag, uint32_t attr) 
{
  Mask::AddToMask(type, flag, 0);
  fAttributes[type] = attr;
}

//____________________________________________________________________
void 
ReadRaw::AttributeMask::RemoveFromMask(Type type)
{
  Mask::RemoveFromMask(type);
  fAttributes[type] = kNone;
}


//____________________________________________________________________
const char**
ReadRaw::AttributeMask::AsTable() const
{
  if (fTable.empty()) {
    for (size_t i = 0; i < fFlags.size(); i++) {
      fTable.push_back(fgkTypes[i]);
      fTable.push_back(fgkFlags[i]);
      std::stringstream s;
      size_t k = 0;
      for (size_t j = 0; j < sizeof(uint32_t) * 8; j++) {
	if (fAttributes[i] & (0x1 << j)) {
	  s << (k != 0 ? " " : "") << j;
	  k++;
	}
      }
      char*       attr = new char[s.str().size()];
      const char* str  = s.str().c_str();
      std::copy(str, &(str[s.str().size()]), attr);
      fTable.push_back(attr);
      fTable.push_back((char*)0);
    }
  }
  return const_cast<const char**>(&(fTable[0]));
}

//____________________________________________________________________
void 
ReadRaw::AttributeMask::Print() const 
{
  std::cout << "Mask is:\n" 
	    << "  Bit pattern: 0x" << std::hex << std::setfill('0')
	    << std::setw(8) << AsBitPattern() << std::dec << std::setfill(' ')
	    << "\n  String table:" << std::endl;
  const char** table = AsTable();
  const char*  p     = table[0];
  while (p) {
    const char* type = p++;
    const char* flag = p++;
    const char* attr = p++;
    std::cout << "    " << type << "=" << flag << " (" << attr << ")" 
	      << std::endl;
  }
}
//____________________________________________________________________
//
// EOF
//


    
