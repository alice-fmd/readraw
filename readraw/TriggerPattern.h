// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    TriggerPattern.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 21:06:21 2006
    @brief   Implementation
*/
#ifndef READRAW_TRIGGERPATTERN_H
#define READRAW_TRIGGERPATTERN_H
#ifndef READRAW_PATTERN_H
# include <readraw/Pattern.h>
#endif

namespace ReadRaw
{
  //__________________________________________________________________
  /** @struct TriggerPattern  readraw/TriggerPattern.h <readraw/TriggerPattern.h>
      Pattern of triggers . 
      @ingroup Data
  */

  struct TriggerPattern : public Pattern
  {
    /** Constructor 
	@param data DAta to read from 
	@param version Event version  */
    TriggerPattern(uint32_t* data=0, uint32_t version=0) 
      : Pattern(data, version) 
    {}
    /** @return The number of words */ 
    size_t N() const { return 2; }
    /** @return Smallest bit */
    size_t Min()  const { return fVersion < 0x00030009 ?  1 :  0; }
    /** @return Largets bit */
    size_t Max()  const { return fVersion < 0x00030009 ? 50 : 49; }
    /** @return Is pattern OK? */
    bool IsOk() const 
    { 
      return (fData[1] & (fVersion < 0x00030009 
			  ? 0xfff80000 : 0x7fe00000)) == 0; 
    }
  };
}

#endif
//
// EOF
//
