// -*- mode: C++ -*- 
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Event.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:24:22 2006
    @brief   Declaration of event structure
*/
#ifndef READRAW_EVENT_H
#define READRAW_EVENT_H
#ifndef READRAW_HEADER_H
# include <readraw/Header.h>
#endif
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef __MAP__
# include <map>
#endif

namespace ReadRaw 
{
  /** @defgroup Data Data classes */
  struct Header;
  
  //==================================================================
  /** @struct Event 
      Container of event information 

      <table>
        <tr><td><b>Header</b></td></tr>
        <tr><td><i>Data</i></td></tr>
      </table>

      @ingroup Data
  */
  struct Event 
  {
    /** List of sub-events */
    typedef std::map<uint32_t, Header*> SubMap;
    /** Constructor */ 
    Event();
    /** Constructor */
    Event(uint32_t* data) { Set(data); }
    /** Destructor */
    virtual ~Event();
    /** Get the event header */
    const Header& Head() const { return fHeader; }
    /** Get the event header */
    Header& Head() { return fHeader; }
    /** Get sub-events */ 
    const SubMap& Sub() const { return fSub; }
    /** Assign from data words */
    bool Set(uint32_t* data);
    /** Clear the event - also deletes the data referenced */ 
    void Clear();
  protected:
    /** The data */ 
    uint32_t* fData;
    /** Header */
    Header fHeader;
    /** List of sub-events */
    SubMap fSub;
    /** Cache of sub-events */ 
    SubMap fCache;
  };
}
#endif
//
// EOF
//

