// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    AltroPrinter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of ALTRO printer 
*/
#ifndef READRAW_ALTROPRINTER_H
#define READRAW_ALTROPRINTER_H
#ifndef READRAW_PRINTER_H
# include <readraw/Printer.h>
#endif
#ifndef READRAW_ALTRODECODER_H
# include <readraw/AltroDecoder.h>
#endif

namespace ReadRaw 
{
  /** 
   * @struct AltroPrinter 
   * Print altro events 
   * @ingroup ALTRO 
   */
  struct AltroPrinter : public Printer, public AltroDecoder
  {
    /** 
     * Contructor.  
     *
     * @param mask Mask of what to print
     * @param out Stream to write to. 
     * @param debug Debug enable
     */
    AltroPrinter(std::ostream& out, uint32_t mask=0x7, bool debug=false) 
      : Printer(out, mask), AltroDecoder(mask & (kData << 3), debug), 
	fIsSodEod(false), fDDL(0)
    {}
    /** 
     * Print contents of event, and possible sub-events. 
     *
     * @param event Event to print
     *
     * @return @c true on success, @c false otherwise 
     */
    virtual bool Visit(const Event& event) { return Printer::Visit(event); }
    /** 
     * Print contents of header, and contained equipment. 
     *
     * @param header Header to print
     *
     * @return @c true on success, @c false otherwise 
     */
    virtual bool Visit(const Header& header);
    /** 
     * Print equipment header information, and contained common data
     * header and data
     *
     * @param equipment The equipment to print 
     *
     * @return @c true on success, @c false otherwise 
     */
    virtual bool Visit(const Equipment& equipment);
    /** 
     * Print the raw data from an equipment. 
     *
     * @param data Raw data from the equipment, packed in 32bit
     *             words. 
     * @param size Size of the data to visit (# of 32 bit words)
     * 
     * @return @c true on success, @c false otherwise 
     */
    virtual bool Visit(const uint32_t* data, const uint32_t size);
    /** 
     * Print the common data header 
     * 
     * @param h Common data header 
     *
     * @return @c true on success, @c false otherwise 
     */
    virtual bool Visit(const CommonHeader& h);
  protected:
    /** 
     * Print a trailer word 
     * 
     * @param addr  Register 'address' 
     * @param val   Value of the register 
     */
    virtual void PrintTrailerWord(unsigned short addr, unsigned int val) ;

    /** Whether this event is a start-of-data or end-of-data event */
    bool fIsSodEod;
    /** Current DDL number */
    unsigned short fDDL;
    /** RCU data format version */
    uint8_t fFormatVersion;
  };
}

#endif
//
// EOF
//

  
