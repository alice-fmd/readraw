//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    dumpddl.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:34:35 2006
    @brief   Program to dump-event information to standard output
*/
#include <readraw/Reader.h>
#include <readraw/Printer.h>
#include <readraw/AltroPrinter.h>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <cerrno>
#include <algorithm>
#include <cstring>
#include "Options.h"

/** @struct to_lower
    Transform a character into lower case. 
 */
struct to_lower 
{
  /** @param c Charactor to transform
      @return  @a c transformed to lower case. */
  char operator()(char c) 
  {
    return std::tolower(c);
  }
};

typedef std::vector<ReadRaw::uint32_t> word_vector;

//____________________________________________________________________
ReadRaw::uint32_t SwapBytes(ReadRaw::uint32_t w) 
{
  return ((w >> 24 & 0x000000ff) || 
	  (w >>  8 & 0x0000ff00) || 
	  (w <<  8 & 0x00ff0000) || 
	  (w << 24 & 0xff000000));
}

//____________________________________________________________________
bool GetNextWord(std::istream& in,  word_vector& ws, bool needSwap=false) 
{
  if (in.eof()) return false;  
  if (!in) throw std::runtime_error("bad stream");
  const size_t n = sizeof(ReadRaw::uint32_t);
  char b[n];
  in.read(b, n);
  ReadRaw::uint32_t* pw = (ReadRaw::uint32_t*)b;
  ReadRaw::uint32_t  w  = *pw;
  if (needSwap) w = SwapBytes(w);
  ws.push_back(w);
  return true;
}

//____________________________________________________________________
/** Main function of @e dumpevents program
    @param argc 
    @param argv 
    @return  */
int 
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<long>        nOpt('n', "events",  "# of events to read", -1);
  Option<long>        sOpt('s', "skip",    "# of events to skip", 0);
  Option<bool>        vOpt('v', "verbose", "Be verbose", false, false);
  Option<bool>        dOpt('d', "debug",   "Show debug info",false,false);
  Option<std::string> wOpt('w', "show",    "What to show", "common,channel");
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(nOpt);
  cl.Add(sOpt);
  cl.Add(vOpt);
  cl.Add(dOpt);
  cl.Add(wOpt);
                                                                               
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    std::cout << "What to show is a comma separated list of one or more of\n\n"
	      << "\tcommon    Show common data header\n"
	      << "\tdata      Dump data\n" 
	      << "\tchannel   Show Channel information\n"
	      << "\tadc       Show ADC information\n"
	      << "\tread      Show Read information\n"
	      << "\ttrailer   Show RCU trailer\n"
	      << std::endl;
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "dumpevents version " 
	      << ReadRaw::Reader::Version() << std::endl;
    return 0;
  }

  // Printer 
  unsigned int      mask  = 0;
  std::stringstream smask(wOpt);
  std::string       token;
  do {
    std::getline(smask, token, ',');
    transform(token.begin(), token.end(), token.begin(), to_lower());
    if      (token == "common")    mask |= ReadRaw::Printer::kCommon;
    else if (token == "data")      mask |= ReadRaw::Printer::kData;
    else if (token == "channel")   mask |= ReadRaw::Printer::kData << 1;
    else if (token == "adc")       mask |= ReadRaw::Printer::kData << 2;
    else if (token == "read")      mask |= ReadRaw::Printer::kData << 3;
    else if (token == "trailer")   mask |= ReadRaw::Printer::kData << 5;
    else {
      std::cerr << "Unknown print option \"" << token << "\"" << std::endl;
      return 1;
    }
  } while  (!smask.eof());
  ReadRaw::AltroPrinter p(std::cout, mask, dOpt);

  // Words and common data header. 
  word_vector           words;
  ReadRaw::CommonHeader cdh;

  // Loop 
  try { 
    typedef std::vector<std::string> string_vector;
    string_vector& remain = cl.Remain();
    for (string_vector::iterator i = remain.begin(); i != remain.end(); ++i) {
      std::cout << "Will open " << *i << std::endl;
      std::ifstream in(i->c_str());
      
      if (!in) { 
	std::cerr << "Failed to open " << *i << ": " 
		  << strerror(errno) << std::endl;
	continue;
      }
      size_t start = in.tellg();
      in.seekg(0, std::ios_base::end);
      size_t end   = in.tellg();
      in.seekg(0, std::ios_base::beg);
      std::cout << "File is " << end-start << " bytes big, or " 
		<< (end-start) / 4 << " 32bit words" << std::endl;
      words.resize(0);
      while (GetNextWord(in, words));

      // Now make a common data header object 
      if (!cdh.Set(&(words[0]))) { 
	std::cout << "Failed to set header!" << std::endl;
	return 1;
      }
      p.Visit(cdh);
      int base   = cdh.Size();
      int nwords = words.size()-base;
      std::cout << "Base: " << base << " nwords: " << nwords << std::endl;
      p.Visit(&(words[base]), nwords);
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}




//____________________________________________________________________
//
// EOF
//

  
