// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Equipment.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:10:35 2006
    @brief   Declaration of Equipment header class
*/
#ifndef READRAW_EQUIPMENT_H
#define READRAW_EQUIPMENT_H
#ifndef READRAW_ATTRIBUTEPATTERN_H
# include <readraw/AttributePattern.h>
#endif
#ifndef READRAW_COMMONHEADER_H
# include <readraw/CommonHeader.h>
#endif
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif

namespace ReadRaw
{
  //==================================================================
  /** @struct Equipment 
      Structure describing a piece of equpment
      @ingroup Data
   */
  struct Equipment 
  {
    /** Constructor */
    Equipment();
    /** Destructor */
    ~Equipment();

    /** @return The size of the equipment block, including common
	event header and data payload */
    uint32_t Size() const { return fData[0]; /* fSize; */ }
    /** @return The type of the equipment as defined in the DATE site */
    uint32_t Type() const { return fData[1]; /* fType; */ }
    /** @return The ID of the equipment as defined in the DATE site */
    uint32_t Id() const   { return fData[2]; /* fId; */  }
    /** @return The type attributes associated to the equipment. */
    const AttributePattern& Attributes() const { return fAttributes; }
    /** @return The size of the basic element accepted by the
	equipment itself. This field is mainly used to adjust the
	content of the payload when crossing endianness boundaries. */
    uint32_t ElementSize() const { return fData[6]; /* fElementSize;*/ }
    /** @return Common header */
    CommonHeader& Head() { return fHeader; }
    /** @return Common header */
    const CommonHeader& Head() const { return fHeader; }
    /** The number of 32bit words in the payload */ 
    const uint32_t DataSize() const { return fN; }
    /** @return Data */
    const uint32_t* Data() const { return fPayload; }
    /** Set the equipment from data words */ 
    bool Set(uint32_t* data);
    /** Clear the equipment */ 
    void Clear();
    /** Whether to verify the common data header */
    static bool fVerify;
  protected: 
    /** Pointer to the data */ 
    uint32_t* fData;
    /** The size of the equipment block, including common event header
	and data payload */
    /** The type attributes associated to the equipment. */
    AttributePattern fAttributes;
    /** @return The size of the basic element accepted by the
	equipment itself. This field is mainly used to adjust the
	content of the payload when crossing endianness boundaries. */
    /** Common data header */
    CommonHeader fHeader;
    /** The data offset */ 
    uint32_t fDataOffset;
    /** The number of 32bit data words in the payload */
    uint32_t fN;
    /** The  actual payload */ 
    uint32_t* fPayload;
  };
}
#endif
//
// EOF
//
