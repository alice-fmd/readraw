// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Printer.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Fri Jun 23 01:29:49 2006
    @brief   Declaration of visitor class 
*/
#ifndef READRAW_PRINTER_H
#define READRAW_PRINTER_H
#ifndef READRAW_VISITOR_H
# include <readraw/Visitor.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace ReadRaw 
{
  /** Class to do hierical indention 
      @ingroup Reader 
  */
  struct Spacer 
  {
  public:
    /** Constructor
        @param dn Indention step */
    Spacer(size_t dn=2) : fN(0), fdN(dn) {}
    /** Destructor */
    ~Spacer() { }
    /** Increase indention */
    Spacer& operator++(int) { fN += fdN; return *this; }
    /** Decrease indention */
    Spacer& operator--(int) { fN = (fN-fdN > 0 ? fN-fdN : 0); return *this;}
    /** @return Curret indent */
    size_t N() const {  return fN; }
    /** Assignment operator 
	@param v Value to set from. 
	@return Reference to @c this object */
    Spacer& operator=(size_t v) { fN = v; return *this; }
  protected:
    /** Current indent */
    size_t fN;
    /** Indention step */
    size_t fdN;
  };

  /** Guard for indention
      @ingroup Reader
  */
  struct SpacerGuard 
  {
    /** Constructor. 
	@param s Spacer object to use */
    SpacerGuard(Spacer& s) : fSpacer(s) { fSpacer++; }
    /** Destructor */
    ~SpacerGuard() { fSpacer--; }
    /** Reference to spacer object */
    Spacer& fSpacer;
  };

  /** @struct Printer 
      Visitor of raw event to print contents of events.
      @ingroup Reader 
   */
  struct Printer : public Visitor
  {
    enum { 
      kEvent      = 0x001, 
      kHeader     = 0x002, 
      kEventId    = 0x004, 
      kTrigger    = 0x008, 
      kDetector   = 0x010, 
      kAttribute  = 0x020, 
      kEquipment  = 0x040, 
      kCommon     = 0x080, 
      kData       = 0x100
    };
    
    /** Contructor.  
	@param out Stream to write to. 
	@param mask Mask of what to show */
    Printer(std::ostream& out, uint32_t mask=0xfffffff) 
      : fOut(out), fMask(mask)
    {}
    /** Destructor */
    virtual ~Printer() {}
    /** Print contents of event, and possible sub-events. 
	@param event Event to print
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const Event& event);
    /** Print contents of header, and contained equipment. 
	@param header Header to print
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const Header& header);
    /** Print event identification
	@param id Identification of event.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const EventId& id);
    /** Print the trigger pattern. 
	@param triggers The trigger pattern.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const TriggerPattern& triggers);
    /** Print the detector pattern. 
	@param detectors The detector pattern.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const DetectorPattern& detectors);
    /** Print the attribute pattern. 
	@param attributes The attribute pattern.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const AttributePattern& attributes);
    /** Print equipment header information, and contained common data
	header and data 
	@param equipment The equipment to print 
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const Equipment& equipment);
    /** Print the common data header of some equipment. 
	@param header The common data header to print.
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const CommonHeader& header);
    /** Print the raw data from an equipment. 
	@param data Raw data from the equipment, packed in 32bit
	words. 
	@param size Number of 32 bit words in @a data
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const uint32_t* data, 
		       const uint32_t  size);
  protected:
    /** Output stream */
    std::ostream& fOut;
    /** Mask of what to show */
    uint32_t fMask;
    /** Spacer. */
    Spacer        fSpacer;
  };
}

/** Output operator for a spacer object 
    @param o Output stream
    @param space Spacer 
    @return @a o after writing @c space.N() spaces to it */
std::ostream& operator<<(std::ostream& o, const ReadRaw::Spacer& space);

#endif
//
// EOF
//
  
  
