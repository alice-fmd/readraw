// -*- mode: C++ -*-
//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Header.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jun 21 22:03:53 2006
    @brief   Declaration of header class 
*/
#ifndef READRAW_HEADER_H
#define READRAW_HEADER_H
#ifndef READRAW_TYPES_H
# include <readraw/Types.h>
#endif
#ifndef READRAW_TRIGGERPATTERN_H
# include <readraw/TriggerPattern.h>
#endif
#ifndef READRAW_DETECTORPATTERN_H
# include <readraw/DetectorPattern.h>
#endif
#ifndef READRAW_ATTRIBUTEPATTERN_H
# include <readraw/AttributePattern.h>
#endif
#ifndef READRAW_EVENTID_H
# include <readraw/EventId.h>
#endif
#ifndef READRAW_EQUIPMENT_H
# include <readraw/Equipment.h>
#endif
#ifndef __CTIME__
# include <ctime>
#endif
#ifndef __MAP__
# include <map>
#endif

namespace ReadRaw
{
  struct Equipment;
  
  //==================================================================
  /**  @struct Header 
       Event or sub-event header information 

       <table>
         <tr><td><tt>Size</tt></td></tr>
         <tr><td><tt>Magic</tt></td></tr>
         <tr><td><tt>Header size</tt></td></tr>
         <tr><td><tt>Version</tt></td></tr>
         <tr><td><tt>Type</tt></td></tr>
         <tr><td><tt>Run number</tt></td></tr>
         <tr><td><b>EventId</b></td></tr>
	 <tr><td><b>TriggerPattern</b></td></tr>
	 <tr><td><b>DetectorPattern</b></td></tr>
	 <tr><td><b>AttributePattern</b></td></tr>
         <tr><td><tt>LDC id</tt></td></tr>
         <tr><td><tt>GDC id</tt></td></tr>
	 <tr><td><tt>Timestamp</tt></td></tr>
	 <tr><td><i>Equipment</i></td></tr>
       </table>

       @ingroup Data
  */
  struct Header 
  {
    /** Map of sub-equipment */
    typedef std::map<uint32_t,Equipment*> EquipmentMap;

    /** Constructor */
    Header() : fData(0) {} 
    /** Destructor */
    virtual ~Header();

    /** The size of the Header structure, in bytes */ 
    static size_t Bytes() { return 68; }
    /** @return The Size */
    uint32_t Size() const { return fData[0]; }
    /** @return The Magic */
    uint32_t Magic() const { return fData[1]; }
    /** @return The HeadSize */
    uint32_t HeadSize() const { return fData[2]; }
    /** @return The Version */
    uint32_t Version() const { return fData[3]; }
    /** @return The Type */
    uint32_t Type() const { return fData[4]; }
    /** @return The RunNb */
    uint32_t RunNb() const { return fData[5]; }
    /** @return The Id */
    const EventId& Id() const { return fId; }
    /** @return The TriggerPattern */
    const TriggerPattern& Triggers() const { return fTriggerPattern; }
    /** @return The DetectorPattern */
    const DetectorPattern& Detectors() const { return fDetectorPattern; }
    /** @return The TypeAttribute */
    const AttributePattern& Attributes() const { return fAttributePattern; }
    /** @return The TypeAttribute */
    AttributePattern& Attributes() { return fAttributePattern; }
    /** @return The LdcId */
    uint32_t LdcId() const { return fData[14]; }
    /** @return The GdcId */
    uint32_t GdcId() const { return fData[15]; }
    /** @return The Timestamp */
    uint32_t Timestamp() const { return fData[16]; }
    /** @return The Timestamp */
    time_t Time() const { return time_t(Timestamp()); }
    /** @return Whether data is byte-swapped */
    bool IsSwapped() const;
    /** Get equipment */
    const EquipmentMap& Equipments() const { return fEquipment; }
     
    /** Set from data words */ 
    bool Set(uint32_t* data);
    /** Clear the header */ 
    void Clear();
  protected:
    /** The data */ 
    uint32_t* fData;
    /** @return The Id */
    EventId fId;
    /** @return The TriggerPattern */
    TriggerPattern fTriggerPattern;
    /** @return The DetectorPattern */
    DetectorPattern fDetectorPattern;
    /** @return The TypeAttribute */
    AttributePattern fAttributePattern;
    /** Map of sub-equipment */
    EquipmentMap fEquipment;
    /** Map of sub-equipment */
    EquipmentMap fCache;
  };
}
#endif
//
// EOF
//
