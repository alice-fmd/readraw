//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    dumprawdata.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:34:35 2006
    @brief   Program to dump-event information to standard output
*/
#include <readraw/Reader.h>
#include <readraw/Printer.h>
#include <readraw/Equipment.h>
#include <readraw/AltroPrinter.h>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include "Options.h"

/** @struct to_lower
    Transform a character into lower case. 
 */
struct to_lower 
{
  /** @param c Charactor to transform
      @return  @a c transformed to lower case. */
  char operator()(char c) 
  {
    return std::tolower(c);
  }
};


/** Main function of @e dumpevents program
    @param argc 
    @param argv 
    @return  */
int 
main(int argc, char** argv)
{
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<long>        nOpt('n', "events",  "# of events to read", -1);
  Option<long>        sOpt('s', "skip",    "# of events to skip", 0);
  Option<bool>        vOpt('v', "verbose", "Be verbose", false, false);
  Option<bool>        dOpt('d', "debug",   "Show debug stuff", false, false);
  Option<bool>        cOpt('c', "verify",  "Verify CDH", false, false);
  Option<std::string> wOpt('w', "show",    "What to show", 
			   "event,header,id,trigger,detector,attribute,equipment,common");
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(nOpt);
  cl.Add(sOpt);
  cl.Add(vOpt);
  cl.Add(wOpt);
  cl.Add(cOpt);
  cl.Add(dOpt);
                                                                               
  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    std::cout << "What to show is a comma separated list of one or more of\n\n"
	      << "\tevent     Show event information\n"
	      << "\theader    Show event header\n" 
	      << "\tid        Show event ID\n" 
	      << "\ttrigger   Show trigger bits\n" 
	      << "\tdetector  Show detector bits\n" 
	      << "\tattribute Show atttribute bits\n" 
	      << "\tequipment Show equipment\n" 
	      << "\tcommon    Show common data header\n"
	      << "\tdata      Dump data\n" 
	      << "\ttrailer   Show RCU trailer\n" 
	      << "\tchannel   Show Channel information\n"
	      << "\tadc       Show ADC information\n"
	      << "\tread      Show Read information\n"
	      << "\tsome      Same as event,header,equipment,channel,data\n"
	      << "\tall       All of the above\n"
	      << std::endl;
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << "dumpevents version " 
	      << ReadRaw::Reader::Version() << std::endl;
    return 0;
  }
  ReadRaw::Equipment::fVerify = cOpt.IsSet();
  try {
    ReadRaw::Reader*      r = 0; 
    if (cl.Remain().size() > 1) 
      r = ReadRaw::Reader::Create(cl.Remain(), vOpt.IsSet(), dOpt.IsSet());
    else if (cl.Remain().size() == 1) 
      r = ReadRaw::Reader::Create((cl.Remain())[0].c_str(), vOpt.IsSet(), dOpt.IsSet());
    else 
      r = ReadRaw::Reader::Create("", vOpt.IsSet(), dOpt.IsSet());
    unsigned int mask  = 0;
    std::stringstream smask(wOpt);
    std::string  token;
    do {
      std::getline(smask, token, ',');
      transform(token.begin(), token.end(), token.begin(), to_lower());
      if      (token == "event")     mask |= ReadRaw::Printer::kEvent;
      else if (token == "header")    mask |= ReadRaw::Printer::kHeader;
      else if (token == "id")        mask |= ReadRaw::Printer::kEventId;
      else if (token == "trigger")   mask |= ReadRaw::Printer::kTrigger;
      else if (token == "detector")  mask |= ReadRaw::Printer::kDetector;
      else if (token == "attribute") mask |= ReadRaw::Printer::kAttribute;
      else if (token == "equipment") mask |= ReadRaw::Printer::kEquipment;
      else if (token == "common")    mask |= ReadRaw::Printer::kCommon;
      else if (token == "data")      mask |= ReadRaw::Printer::kData;
      else if (token == "channel")   mask |= ReadRaw::Printer::kData << 1;
      else if (token == "adc")       mask |= ReadRaw::Printer::kData << 2;
      else if (token == "read")      mask |= ReadRaw::Printer::kData << 3;
      else if (token == "foo")       mask |= ReadRaw::Printer::kData << 4;
      else if (token == "trailer")   mask |= ReadRaw::Printer::kData << 5;
      else if (token == "some")       
	mask |= (ReadRaw::Printer::kData << 1 |
		 ReadRaw::Printer::kData << 5 |
		 ReadRaw::Printer::kEquipment |
		 ReadRaw::Printer::kHeader    |
		 ReadRaw::Printer::kEvent);
      else if (token == "all")       
	mask |= (ReadRaw::Printer::kData << 1 |
		 ReadRaw::Printer::kData << 2 |
		 ReadRaw::Printer::kData << 3 |
		 ReadRaw::Printer::kData << 5 |
		 ReadRaw::Printer::kData      |
		 ReadRaw::Printer::kCommon    |
		 ReadRaw::Printer::kEquipment |
		 ReadRaw::Printer::kAttribute |
		 ReadRaw::Printer::kDetector  |
		 ReadRaw::Printer::kTrigger   |
		 ReadRaw::Printer::kEventId   |
		 ReadRaw::Printer::kHeader    |
		 ReadRaw::Printer::kEvent);
      else {
	std::cerr << "Unknown print option \"" << token << "\"" << std::endl;
	return 1;
      }
    } while  (!smask.eof());
    
    ReadRaw::AltroPrinter p(std::cout, mask, dOpt);
    ReadRaw::Event        e;
    ReadRaw::Mask         m;
    // m.AddToMask(ReadRaw::Mask::kPhysicsEvent, ReadRaw::Mask::kAll);
    m.AddToMask(ReadRaw::Mask::kAllEvents, ReadRaw::Mask::kAll);
    r->SetMask(m);
    r->SetVerbose(vOpt);
    p.SetVerbose(vOpt);
    long i = 0;
    while (!r->IsEOD() && (nOpt < 0 || i < nOpt+sOpt)) {
      bool ret = r->GetNextEvent(e);
      if (i >= sOpt) {
	if (!ret) continue;
	p.Visit(e);
      }
      // delete e;
      i++;
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}




//____________________________________________________________________
//
// EOF
//

  
