//____________________________________________________________________
//
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Event.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Jun 22 01:45:22 2006
    @brief   Implementation of Event class 
*/
#include "Event.h"
#include <iostream>
#include <sstream>
#include <stdexcept>
  
//____________________________________________________________________
ReadRaw::Event::~Event()
{
  Clear();
  for (SubMap::iterator i = fCache.begin(); i != fCache.end(); ++i) {
    delete (*i).second;
  }
}

//____________________________________________________________________
ReadRaw::Event::Event()
  : fData(0)
{}

//____________________________________________________________________
bool
ReadRaw::Event::Set(uint32_t* data)
{
  bool status = true;
  fData = data;
  if (!(status = fHeader.Set(fData))) 
    std::cerr << "Event: Failed to set header" << std::endl;

  if (!fHeader.Attributes().IsSuperEvent()) return status;
  
  const size_t   nbytes   = sizeof(uint32_t);
  size_t         dataSize = (fHeader.Size() - Header::Bytes());
  size_t         read     = 0;
  uint32_t*      ptr      = &(data[Header::Bytes() / nbytes]);
  while (read < dataSize) {
    uint32_t               ldc =  ptr[14];
    Header*                h   =  0;
    SubMap::iterator       i   =  fCache.find(ldc);
    if (i == fCache.end()) h   =  fCache[ldc] = new Header;
    else                   h   =  i->second;
    fSub[ldc]                  =  h;
    h->Clear();
    if (!(status = h->Set(ptr))) 
      std::cerr << "Event: Failed to set sub-event header" << std::endl;
    ptr                        += h->Size() / nbytes;
    read                       += h->Size();
  }
  if (read != 0) {
    std::stringstream s;
    s << "Event: Read header and equipment doesn't add up to " 
      << fHeader.Size() << " but " << read + 68;
    throw std::runtime_error(s.str().c_str());
  }
  return status;
}

//____________________________________________________________________
void
ReadRaw::Event::Clear()
{
  if (fData) delete [] fData;
  fHeader.Clear();
  fSub.clear();
}


//____________________________________________________________________
//
// EOF
//
