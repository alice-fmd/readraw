/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Mainpage.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage Read Raw data as formated by DATE

    This is a package to read raw data files, as produced by date, 
    and put it in to a class structure reflecting the events, their
    sub-events, equipments, common data headers, and raw numbers. 

    The library implements a visitor pattern (well, almost) for the
    object hierarcy, to make it easy to pass over the data and decode
    it. 
    
    The library is implemented stand-alone, so one can install it with
    out having to install the full DATE package.  This is especially
    useful on platforms where DATE is not supported (MacOSX, Windows,
    etc.) or for machines that are not meant to do on-line
    processing. 

    The object hierarcy is shown below 
    
    @dotfile obj_hier.doc "Object hierarcy" 

    A special @e visitor class ReadRaw::Visitor is defined to ease
    parsing the online data.   See for example the ReadRaw::Printer
    and ReadRaw::AltroPrinter class for examples. 
*/

#error Not for compilation
//
// EOF
//
